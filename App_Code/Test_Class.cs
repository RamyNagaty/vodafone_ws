﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;

/// <summary>
/// Summary description for Test_Class
/// </summary>
public class Test_Class
{
    OracleAcess oracleacess = new OracleAcess("Bill_App_Conn");
    string SQL = "";
    DataSet Ds = new DataSet();
    DataTable dt = new DataTable();

	public Test_Class()
	{
		//
		// TODO: Add constructor logic here
		//
	}


    #region "Basic Data"
    //*************** BASic COde*********************** tareq
    //[WebMethod]
    public DataSet GetBacicCode_branchs()
    {
        //branchs
        SQL = " Select * FROM branchs";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetBacicCode_GOVS()
    {
        // GOVS
        SQL = " Select * FROM GOVS";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetBacicCode_Productgroup()
    {
        //Productgroup
        SQL = " Select * FROM ITEM_GROUPS order by CODE";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetBacicCode_CITIES()
    {
        //CITIES
        SQL = " Select * FROM CITIES";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetBacicCode_EG_ITEMS()
    {
        //EG_ITEMS
        //SQL = " Select CODE, NAME, POINTS, product_code(ITEM_TYPE) as ITEM_TYPE FROM EG_ITEMS Where Active='Y' AND Points>0 AND PDA='Y'";
        SQL = " Select CODE, NAME, POINTS, product_code(ITEM_TYPE) as ITEM_TYPE FROM EG_ITEMS Where Active='Y' AND PDA='Y'";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetBacicCode_EG_SHOPS()
    {
        //EG_SHOPS
        SQL = " Select CODE, NAME, OWNER_NAME, GOV_CODE FROM EG_SHOPS Where GOV_CODE >0";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetBacicCode_DISTRICT()
    {
        //DISTRICT
        SQL = " Select CODE, CITY_CODE, NAME FROM DISTRICT";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetBacicCode_EG_Gifts()
    {
        // Gifts
        SQL = " Select CODE, NAME, Points, ACTIVE, Branch FROM EG_Gifts";
        Ds.Tables.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetViewingSteps()
    {
        //ViewingSteps
        return Ds;
    }

    //[WebMethod]
    public DataSet GetViewingEG_CANCEL_REASONS()
    {
        try
        {
            SQL = " Select * FROM EG_CANCEL_REASONS";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
            return Ds;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "rejectViewing");
            return Ds;
        }
    }

    //[WebMethod]
    public DataSet GetViewingEG_V_DESC()
    {
        SQL = " Select NAME FROM EG_V_DESC";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetViewingJOKER_GIFTS()
    {
        SQL = " Select Code, NAME FROM JOC_GIFTS";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetViewingEG_GUR_REASONS()
    {
        SQL = " Select Code, NAME FROM EG_GUR_REASONS";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetViewingEG_TITLES()
    {
        SQL = " Select CODE, TITLE FROM EG_TITLES";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //***************END BASic COde*********************** tareq
    #endregion

    //**************************************************
    //[WebMethod]
    public String Get_ROMKO_Name(string Emp_Code)
    {
        string ret_val;
        SQL = "SELECT NAME FROM ROMKO WHERE CODE =" + Emp_Code;
        object Pno = oracleacess.ExecuteScalar(SQL);
        ret_val = Pno.ToString();
        return ret_val;
    }

    //*************** VIEWING *********************** //******* GET VIEWING

    //[WebMethod]
    public DataSet GetViewing(int Rep_COde)
    {
        //Select Viewing
        SQL = " SELECT EG_VIEWING.CODE, EG_VIEWING.CUST_CODE, EG_VIEWING.ADDRESS, EG_VIEWING.PLN_DATE, EG_VIEWING.OWNER_NAME, EG_VIEWING.TEL, ";
        SQL += " EG_VIEWING.MOB, EG_VIEWING.FEEDBACK, EG_CUSTOMERS.NAME, EG_CUSTOMERS.TEL AS CUSTOMERSTel,EG_CUSTOMERS.TEL2 AS CUSTOMERSTel2, EG_CUSTOMERS.MOBILE, EG_CUSTOMERS.ID_NO,EG_CUSTOMERS.FIRSTS, ";
        SQL += " EG_VIEWING.TITLE, EG_VIEWING.GOV_CODE, EG_VIEWING.CITY_CODE, EG_VIEWING.DIST_CODE, jok_count(CUST_CODE) as AddCard, EG_VIEWING.NOTES, ";
        SQL += " EG_VIEWING.GROUP_mang_code";
        SQL += " , STOP_CERT, STOP_RSN, WC_CNT, KITCHEN_CNT, VIEW_TYPE, PROBLEM ";
        SQL += " FROM EG_VIEWING INNER JOIN ";
        SQL += " EG_CUSTOMERS ON EG_VIEWING.CUST_CODE = EG_CUSTOMERS.CODE Where EG_VIEWING.PLN_DATE=Trunc(sysdate) AND REP_CODE=" + Rep_COde + " AND PDA_Status = 0";
        Ds.Tables.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public bool Change_PDA_Status4GetViewing(int Rep_COde)
    {
        SQL = " Update EG_VIEWING set PDA_Status=1 Where PLN_DATE=Trunc(sysdate) AND EG_VIEWING.REP_CODE="
            + Rep_COde + " AND EG_VIEWING.PDA_Status=0";
        oracleacess.ExecuteNonQuery(SQL);
        return true;
    }

    //[WebMethod]//لإرجاع المعاينات
    public void ReturnViewing(int Rep_COde)
    {
        SQL = " Update EG_VIEWING SET PDA_Status=0 Where PLN_DATE=Trunc(sysdate) AND EG_VIEWING.REP_CODE="
            + Rep_COde + " AND EG_VIEWING.PDA_Status>0";
        oracleacess.ExecuteNonQuery(SQL);
    }

    /// <summary>
    /// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /// </summary>
    /// <param name="code"></param>
    /// <param name="reason"></param>
    /// <param name="state"></param>
    /// <returns></returns>
    //[WebMethod]
    public int rejectViewing(int code, string reason, int state)
    {
        try
        {
            SQL = " Update EG_VIEWING SET PDA_Status = 3";
            SQL += " ,Cancel_Reason=MY_Reverse('" + reason + "')";

            if (state == 1)
            {
                SQL += ",ORDER_STATUS='مرفوضة'";
            }
            else if (state == 2)
            {
                //SQL += ",VIEW_STATUS='تم التأجيل'";
                SQL += ",ORDER_STATUS='تم التأجيل'";
            }
            SQL += " , SEND_TIME=to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS') ";
            SQL += " Where CODE=" + code + "";
            return oracleacess.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "rejectViewing");
            return -1;
        }
    }
    //[WebMethod]
    public int rejectViewing_prob(int code, string reason, int state, int rep_code, string to_code)
    {
        try
        {
            //update eg_viewing set feedback = concat((select feedback from eg_viewing where code =3047664 ),'qwe') where code = 3047664
            SQL = " Update EG_VIEWING SET PDA_Status = 3 , rep_code =" + to_code;
            SQL += ",feedback = concat('محولة من " + rep_code + " ',(select feedback from eg_viewing where code = " + code + "))";
            SQL += " ,Cancel_Reason=MY_Reverse('" + reason + "')";
            //SQL += ",ORDER_STATUS=MY_Reverse('" + state + "')";

            if (state == 1)
            {
                //SQL += ",VIEW_STATUS='مرفوضة'";
                SQL += ",ORDER_STATUS='مرفوضة'";
            }
            else if (state == 2)
            {
                //SQL += ",VIEW_STATUS='تم التأجيل'";
                SQL += ",ORDER_STATUS='تم التأجيل'";
            }
            SQL += " , SEND_TIME=to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS') ";
            SQL += " Where CODE=" + code + "";
            return oracleacess.ExecuteNonQuery(SQL);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "rejectViewing");
            return -1;
        }
    }
    #region "Update CRM"
    //[WebMethod]
    public bool UPdateViewings2(DataSet DTViewingS) //public bool UPdateViewings()
    {

        #region EG_VIEWING
        try
        {
            object vcode = 0;
            for (int row = 0; row < DTViewingS.Tables[0].Rows.Count; row++)
            {
                if (Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["PRN_NO"]) == 1)
                {
                    SQL = "";
                    if (int.Parse(DTViewingS.Tables[0].Rows[row]["GUR_STATUS"].ToString()) == 1)
                    {
                        SQL = " Update EG_VIEWING SET GUR_STATUS='Y'";
                    }
                    else if (int.Parse(DTViewingS.Tables[0].Rows[row]["GUR_STATUS"].ToString()) == 0)
                    {
                        SQL = " Update EG_VIEWING SET GUR_STATUS='N'";
                    }
                    SQL += " ,GUR_REASON=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["GUR_REASON"].ToString() + "')";
                    if (DTViewingS.Tables[0].Rows[row]["POINTS"] == DBNull.Value)
                    {
                        SQL += " ,POINTS=0";
                    }
                    else
                    {
                        SQL += " ,POINTS = " + Convert.ToDouble(DTViewingS.Tables[0].Rows[row]["POINTS"]).ToString() + " ";
                    }

                    SQL += " ,V_DESC=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["V_DESC"].ToString() + "')";
                    SQL += " ,CANCEL_REASON=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["VIEW_Reason"].ToString() + "')";
                    if (DTViewingS.Tables[0].Rows[row]["Level"] == DBNull.Value)
                    {
                        SQL += " ,View_level=0";
                    }
                    else
                    {
                        SQL += " ,View_level=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["Level"]).ToString() + "";
                    }
                    SQL += " ,PDA_Status = 3";
                    SQL += " ,ADDRESS=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["ADDRESS"] + "')";
                    SQL += " ,OWNER_NAME=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["OWNER_NAME"] + "')";
                    SQL += " ,ORDER_STATUS='" + DTViewingS.Tables[0].Rows[row]["VIEW_STATUS"] + "'";
                    SQL += " ,TEL='" + DTViewingS.Tables[0].Rows[row]["TEL"] + "'";
                    SQL += " ,MOB='" + DTViewingS.Tables[0].Rows[row]["MOB"] + "'";
                    if (DTViewingS.Tables[0].Rows[row]["SHOP_CODE"] == DBNull.Value)
                    {
                        SQL += " ,SHOP_CODE=0 ";
                    }
                    else
                    {
                        SQL += " ,SHOP_CODE=" + DTViewingS.Tables[0].Rows[row]["SHOP_CODE"] + "";
                    }
                    SQL += " ,TITLE=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["TITLE"] + "')";

                    SQL += " ,De_NOTES=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["De_NOTE"] + "')";
                    SQL += " ,NOTES=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["NOTES"] + "')";
                    SQL += " ,Shop_Note=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["Shop_Note"] + "')";

                    SQL += " ,Until_Work_Comp=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["Until_Work_Comp"] + "')";
                    if (Convert.ToBoolean(DTViewingS.Tables[0].Rows[row]["FlageID"]) == true)
                    {
                        SQL += " ,CUST_CODE=" + Convert.ToInt32(vcode.ToString()) + "";
                        SQL += ",FLAG = 1";
                    }
                    else
                    {
                        SQL += " ,CUST_CODE=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["CUST_CODE"]).ToString() + "";
                    }
                    SQL += " ,PRN_No=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["CODE"]).ToString() + "";

                    // Update in 20-12-2011 New Request by Assmaa & Essam & Ramy 
                    //, STOP_CERT, STOP_RSN, WC_CNT, KITCHEN_CNT, VIEW_TYPE

                    //STOP_CERT
                    //LogError("1 Test Eslam STOP_CERT",(DTViewing.Tables[0].Rows[row]["STOP_CERT"].ToString()));
                    if (DTViewingS.Tables[0].Rows[row]["STOP_CERT"].ToString() == bool.TrueString)
                    {
                        SQL += " , STOP_CERT='Y'";
                    }
                    else if (DTViewingS.Tables[0].Rows[row]["STOP_CERT"].ToString() == bool.FalseString)
                    {
                        SQL += " , STOP_CERT='N'";
                    }

                    //STOP_RSN
                    //LogError("2 Test Eslam STOP_RSN", (DTViewing.Tables[0].Rows[row]["STOP_RSN"].ToString()));
                    SQL += " , STOP_RSN=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["STOP_RSN"].ToString() + "')";

                    //WC_CNT
                    //LogError("3 Test Eslam WC_CNT", (DTViewing.Tables[0].Rows[row]["WC_CNT"].ToString()));
                    SQL += " , WC_CNT=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["WC_CNT"]).ToString() + "";

                    //KITCHEN_CNT
                    //LogError("4 Test Eslam KITCHEN_CNT", (DTViewing.Tables[0].Rows[row]["KITCHEN_CNT"].ToString()));
                    SQL += " ,KITCHEN_CNT=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["KITCHEN_CNT"]).ToString() + "";

                    //VIEW_TYPE
                    //LogError("5 Test Eslam VIEW_TYPE", (DTViewing.Tables[0].Rows[row]["VIEW_TYPE"].ToString()));
                    SQL += " , VIEW_TYPE=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["VIEW_TYPE"].ToString() + "')";
                    //End Update in 20-12-2011 New Request by Assmaa & Essam & Ramy 
                    //SQL += " , SEND_TIME='" + DTViewingS.Tables[0].Rows[row]["SEND_TIME"] + "'";
                    SQL += " , SEND_TIME=to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"))
                                        + "','MM/DD/YYYY HH24:MI:SS') ";

                    SQL += " Where CODE=" + DTViewingS.Tables[0].Rows[row]["CODE"] + "";
                    SQL += " And View_Serial=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["PRN_No"]).ToString() + "";
                    oracleacess.ExecuteNonQuery(SQL);
                }
                else
                #region else
                {

                    SQL = " Insert INTO EG_VIEWING(";
                    SQL += "  CODE, PRN_No, CUST_CODE, FLAG, ADDRESS, PLN_DATE, FROM_TIME, TO_TIME";
                    SQL += ", OWNER_NAME, TEL, MOB";
                    SQL += ", FEEDBACK, VIEW_SERIAL, GUR_STATUS, GUR_REASON, POINTS, V_DESC, CANCEL_REASON, View_level, PDA_Status";
                    SQL += ", ORDER_STATUS, SHOP_CODE, TITLE, De_NOTES, NOTES, Shop_Note, Until_Work_Comp, Rep_Code, GROUP_mang_code, ";
                    SQL += "GOV_CODE, CITY_CODE, DIST_CODE ";

                    // Update in 20-12-2011 New Request by Assmaa & Essam & Ramy
                    SQL += ",STOP_CERT, STOP_RSN, WC_CNT, KITCHEN_CNT, VIEW_TYPE , SEND_TIME";
                    //End Update in 20-12-2011 New Request by Assmaa & Essam & Ramy 

                    SQL += " )";
                    SQL += " VALUES(";
                    SQL += "  " + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["CODE"].ToString()) + "";
                    SQL += ",  " + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["CODE"].ToString()) + "";
                    if (Convert.ToBoolean(DTViewingS.Tables[0].Rows[row]["FlageID"]) == true)
                    {
                        SQL += ", " + Convert.ToInt32(vcode.ToString()) + "";
                        SQL += ",1";
                    }
                    else
                    {
                        SQL += ", " + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["CUST_CODE"].ToString()) + "";
                        SQL += ",0";
                    }
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["ADDRESS"].ToString() + "')";
                    SQL += ",to_date('" + (Convert.ToDateTime(DTViewingS.Tables[0].Rows[row]["PLN_DATE"]).ToString("MM/dd/yyyy HH:mm:ss"))
                                        + "','MM/DD/YYYY HH24:MI:SS') ";
                    SQL += ",to_date('" + (Convert.ToDateTime(DTViewingS.Tables[0].Rows[row]["FROM_TIME"]).ToString("MM/dd/yyyy HH:mm:ss"))
                                        + "','MM/DD/YYYY HH24:MI:SS') ";
                    SQL += ",to_date('" + (Convert.ToDateTime(DTViewingS.Tables[0].Rows[row]["TO_TIME"]).ToString("MM/dd/yyyy HH:mm:ss"))
                                        + "','MM/DD/YYYY HH24:MI:SS') ";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["OWNER_NAME"].ToString() + "')";
                    SQL += ",'" + DTViewingS.Tables[0].Rows[row]["TEL"].ToString() + "'";
                    SQL += ",'" + DTViewingS.Tables[0].Rows[row]["MOB"].ToString() + "'";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["NOTES"].ToString() + "')";
                    SQL += ",'" + DTViewingS.Tables[0].Rows[row]["PRN_NO"].ToString() + "'";
                    if (int.Parse(DTViewingS.Tables[0].Rows[row]["GUR_STATUS"].ToString()) == 1)
                    {
                        SQL += " ,'Y'";
                    }
                    else if (int.Parse(DTViewingS.Tables[0].Rows[row]["GUR_STATUS"].ToString()) == 0)
                    {
                        SQL += " ,'N'";
                    }
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["GUR_REASON"].ToString() + "')";
                    if (DTViewingS.Tables[0].Rows[row]["POINTS"] == DBNull.Value)
                    {
                        SQL += ",0";
                    }
                    else
                    {
                        SQL += "," + Convert.ToDouble(DTViewingS.Tables[0].Rows[row]["POINTS"]).ToString() + "";
                    }
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["V_DESC"].ToString() + "')";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["VIEW_Reason"].ToString() + "')";
                    if (DTViewingS.Tables[0].Rows[row]["Level"] == DBNull.Value)
                    {
                        SQL += ",0";
                    }
                    else
                    {
                        SQL += "," + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["Level"]).ToString() + "";
                    }
                    SQL += ",3";
                    SQL += " , '" + DTViewingS.Tables[0].Rows[row]["VIEW_STATUS"] + "'";
                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["SHOP_CODE"] + "";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["TITLE"] + "')";

                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["De_NOTE"] + "')";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["NOTES"] + "')";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["Shop_Note"] + "')";

                    SQL += ",'" + DTViewingS.Tables[0].Rows[row]["Until_Work_Comp"] + "'";
                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["RepCode"] + "";
                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["GROUP_mang_code"] + "";

                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["GOV_CODE"] + "";
                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["CITY_CODE"] + "";
                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["DIST_CODE"] + "";

                    // Update in 20-12-2011 New Request by Assmaa & Essam & Ramy 
                    //, STOP_CERT, STOP_RSN, WC_CNT, KITCHEN_CNT, VIEW_TYPE                     
                    //STOP_CERT
                    if (int.Parse(DTViewingS.Tables[0].Rows[row]["STOP_CERT"].ToString()) == 1)
                    {
                        SQL += " ,'Y'";
                    }
                    else if (int.Parse(DTViewingS.Tables[0].Rows[row]["STOP_CERT"].ToString()) == 0)
                    {
                        SQL += " ,'N'";
                    }
                    //STOP_RSN
                    SQL += " ,MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["STOP_RSN"] + "')";
                    //WC_CNT
                    SQL += " , " + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["WC_CNT"]).ToString() + "";
                    //KITCHEN_CNT
                    SQL += " , " + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["KITCHEN_CNT"]).ToString() + "";
                    //VIEW_TYPE
                    SQL += " , MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["VIEW_TYPE"] + "')";
                    //End Update in 20-12-2011 New Request by Assmaa & Essam & Ramy 
                    //SQL += " , '"+ DTViewingS.Tables[0].Rows[row]["SEND_TIME"] +"'";
                    SQL += " , to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"))
                                        + "','MM/DD/YYYY HH24:MI:SS') ";
                    SQL += " )";
                    oracleacess.ExecuteNonQuery(SQL);
                }
                #endregion else
                ////////////////////////////////////////////////////////////////////////////////

            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateViewings");
            return false;
        }
        #endregion EG_viewing
        ///////////////////////////////////////////////////////////////////////////////////
        #region EG_view_steps
        try
        {
            if (DTViewingS.Tables[1].Rows.Count > 0)
            {
                for (int r = 0; r < DTViewingS.Tables[1].Rows.Count; r++)
                {
                    SQL = "delete from EG_View_Steps where view_code = " + DTViewingS.Tables[1].Rows[r]["View_Code"];
                    oracleacess.ExecuteNonQuery(SQL);
                }
                for (int row2 = 0; row2 < DTViewingS.Tables[1].Rows.Count; row2++)
                {
                    SQL = " Insert INTO EG_View_Steps(View_Code, step_NO, VIEW_SERIAL, Step_Time, AVLFlag)values("
                        + Convert.ToInt32(DTViewingS.Tables[1].Rows[row2]["View_Code"].ToString()) + "";
                    SQL += "," + Convert.ToInt32(DTViewingS.Tables[1].Rows[row2]["step_NO"].ToString()) + "";
                    SQL += "," + Convert.ToInt32(DTViewingS.Tables[1].Rows[row2]["PRN_NO"].ToString()) + "";
                    SQL += ",to_date('" + (Convert.ToDateTime(DTViewingS.Tables[1].Rows[row2]["Step_Time"]).ToString("MM/dd/yyyy HH:mm:ss"))
                        + "', 'MM/DD/YYYY HH24:MI:SS')" + "";
                    //LogError("1- Test UPdateEG_View_Steps", (Convert.ToDateTime(DTViewingEG_View_Steps.Tables[0].Rows[row2]["Step_Time"]).ToString("MM/dd/yyyy HH:mm:ss")));
                    //LogError("2- Test UPdateEG_View_Steps", (Convert.ToDateTime(DTViewingEG_View_Steps.Tables[0].Rows[row2]["Step_Time"]).ToString("MM/dd/yyyy HH:mm:ss")));
                    SQL += "," + Convert.ToInt32(DTViewingS.Tables[1].Rows[row2]["AVLFlag"].ToString()) + " )";
                    //LogError("UPdateEG_View_Steps", SQL);
                    oracleacess.ExecuteNonQuery(SQL);
                }
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateEG_View_Steps");
            return false;
        }
        #endregion EG_view_steps
        ////////////////////////////////////////////////////////////////////////////////
        #region EG_viewing_Det
        try
        {
            object det_code = 0;
            for (int r = 0; r < DTViewingS.Tables[2].Rows.Count; r++)
            {
                SQL = "delete from EG_VIEWING_DET where view_code = " + DTViewingS.Tables[2].Rows[r]["View_Code"];
                oracleacess.ExecuteNonQuery(SQL);
            }
            for (int row1 = 0; row1 < DTViewingS.Tables[2].Rows.Count; row1++)
            {
                //SQL = " Select max(CODE)+1 FROM EG_VIEWING_DET";
                //code = oracleacess.ExecuteScalar(SQL);

                SQL = " Insert INTO EG_VIEWING_DET(CODE, VIEW_CODE, ITEM_CODE, VIEW_SERIAL, QTY, POINTS, TOTAL_POINTS)Values("
                     + "View_DET_SEQ.NextVal, "
                    //+ Convert.ToInt32(code.ToString()) + ","
                     + Convert.ToInt32(DTViewingS.Tables[2].Rows[row1]["VIEW_CODE"].ToString())
                     + "," + Convert.ToInt32(DTViewingS.Tables[2].Rows[row1]["ITEM_CODE"].ToString())
                     + "," + Convert.ToInt32(DTViewingS.Tables[2].Rows[row1]["PRN_NO"].ToString())
                     + "," + Convert.ToDouble(DTViewingS.Tables[2].Rows[row1]["QTY"].ToString()) + ""
                     + "," + Convert.ToDouble(DTViewingS.Tables[2].Rows[row1]["POINTS"].ToString()) + ","
                     + (Convert.ToDouble(DTViewingS.Tables[2].Rows[row1]["POINTS"].ToString())
                     * Convert.ToDouble(DTViewingS.Tables[2].Rows[row1]["QTY"].ToString())) + ")";
                oracleacess.ExecuteNonQuery(SQL);
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateViewingDTL");
            return false;
        }
        #endregion EG_viewing_Det
        return true;
    }
    //[WebMethod]
    public bool Update_Hasr(DataSet ds)
    {

        try
        {
            for (int r = 0; r < ds.Tables[0].Rows.Count; r++)
            {
                SQL = "delete from HS_VIEW_PROD where view_code = " + ds.Tables[0].Rows[r]["View_Code"];
                oracleacess.ExecuteNonQuery(SQL);
            }
            for (int row2 = 0; row2 < ds.Tables[0].Rows.Count; row2++)
            {

                object hs_code = 0;
                SQL = " SELECT MAX(CODE)+1 FROM HS_VIEW_PROD ";
                hs_code = oracleacess.ExecuteScalar(SQL);

                if (hs_code == DBNull.Value)
                {
                    hs_code = 1;
                }

                SQL = null;
                SQL += " INSERT INTO HS_VIEW_PROD (CODE, GROUP_CODE, PROD_CODE, DIM_CODE, VIEW_CODE, VIEW_SERIAL, PROD_CNT,tot,tot_val)";
                SQL += " VALUES (";

                SQL += "" + hs_code.ToString() + "";
                SQL += "," + int.Parse(ds.Tables[0].Rows[row2]["GROUP_CODE"].ToString()) + "";
                SQL += "," + ds.Tables[0].Rows[row2]["PROD_CODE"].ToString() + "";
                SQL += "," + ds.Tables[0].Rows[row2]["DIM_CODE"].ToString() + "";
                SQL += "," + ds.Tables[0].Rows[row2]["VIEW_CODE"].ToString() + "";
                SQL += "," + ds.Tables[0].Rows[row2]["VIEW_SERIAL"].ToString() + "";

                SQL += "," + ds.Tables[0].Rows[row2][6].ToString() + "";
                SQL += "," + ds.Tables[0].Rows[row2][7].ToString() + "";//total points
                SQL += "," + ds.Tables[0].Rows[row2][8].ToString() + "";//total price
                SQL += ")";
                oracleacess.ExecuteNonQuery(SQL);
                //SQL = null;
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdate_HS_VIEW_PROD");
            return false;
        }

    }
    //[WebMethod]
    public bool Update_problems(DataSet DTViewingS)
    {
        try
        {
            object prob_code = 0;
            for (int r = 0; r < DTViewingS.Tables[4].Rows.Count; r++)
            {
                SQL = "delete from QA_RESP_PROB where view_code = " + DTViewingS.Tables[4].Rows[r]["View_Code"];
                oracleacess.ExecuteNonQuery(SQL);
            }
            for (int row = 0; row < DTViewingS.Tables[4].Rows.Count; row++)
            {

                SQL = " SELECT MAX(CODE)+1 FROM QA_RESP_PROB ";
                prob_code = oracleacess.ExecuteScalar(SQL);
                if (prob_code == DBNull.Value)
                {
                    prob_code = 1;
                }
                SQL = null;
                SQL += "insert into QA_RESP_PROB (CODE , VIEW_CODE , PROB_TEAM_CODE, PROB_HELP ,PROB_KIND,PROB_TYPE,CUST_CODE,CUST_MOBILE,";
                SQL += "OWNER,OWNER_MOBILE,ADDRESS,PLN_DATE,INIT_VAL ) VALUES (";
                SQL += prob_code.ToString() + ",";
                SQL += DTViewingS.Tables[4].Rows[row]["VIEW_CODE"].ToString() + ",";
                SQL += DTViewingS.Tables[4].Rows[row]["RESP_CODE"].ToString() + ",";
                SQL += DTViewingS.Tables[4].Rows[row]["PROB_HELP"].ToString() + ",";
                SQL += "MY_Reverse('" + DTViewingS.Tables[4].Rows[row]["PROB_KIND"].ToString() + "'),";
                SQL += "MY_Reverse('" + DTViewingS.Tables[4].Rows[row]["PROB_DESC"].ToString() + "'),";
                SQL += DTViewingS.Tables[4].Rows[row]["CUST_CODE"].ToString() + ",'";
                SQL += DTViewingS.Tables[4].Rows[row]["CUST_MOB"].ToString() + "',";
                SQL += "MY_Reverse('" + DTViewingS.Tables[4].Rows[row]["OWNER_NAME"].ToString() + "'),'";
                SQL += DTViewingS.Tables[4].Rows[row]["OWNER_MOB"].ToString() + "',";
                SQL += "MY_Reverse('" + DTViewingS.Tables[4].Rows[row]["OWNER_ADDRESS"].ToString() + "'),";
                SQL += "to_date('" + (Convert.ToDateTime(DTViewingS.Tables[4].Rows[row]["PROB_DATE"]).ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS'),";
                SQL += DTViewingS.Tables[4].Rows[row]["TOT_COST"] + ")";
                oracleacess.ExecuteNonQuery(SQL);
                SQL = "delete from QA_PROB_ITEMS where prob_code = " + prob_code;
                oracleacess.ExecuteNonQuery(SQL);
                SQL = "delete from QA_PROB_COST where prob_code = " + prob_code;
                oracleacess.ExecuteNonQuery(SQL);
            }

            ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            if (DTViewingS.Tables.Count > 4)
            {
                object prob_c_code = 0;
                try
                {

                    for (int row = 0; row < DTViewingS.Tables[5].Rows.Count; row++)
                    {

                        SQL = " SELECT MAX(CODE)+1 FROM QA_PROB_ITEMS ";
                        prob_c_code = oracleacess.ExecuteScalar(SQL);
                        if (prob_c_code == DBNull.Value)
                        {
                            prob_c_code = 1;
                        }
                        //SQL = " SELECT MAX(CODE) FROM QA_RESP_PROB ";
                        //object prob_code = oracleacess.ExecuteScalar(SQL);
                        SQL = null;
                        SQL += "insert into QA_PROB_ITEMS (CODE , PROB_CODE , ITEM_CODE ,QTY,price,tot )VALUES (";
                        SQL += prob_c_code.ToString() + "," + prob_code + ",";
                        SQL += DTViewingS.Tables[5].Rows[row]["ITEM_CODE"].ToString() + ",";
                        SQL += DTViewingS.Tables[5].Rows[row]["qty"].ToString() + ",";
                        SQL += DTViewingS.Tables[5].Rows[row]["price"].ToString() + ",";
                        SQL += DTViewingS.Tables[5].Rows[row]["tot"].ToString();
                        SQL += ")";
                        oracleacess.ExecuteNonQuery(SQL);
                    }
                }
                catch (Exception ex)
                {
                    LogError(ex.Message, "upd_prob_items_new");
                    return false;
                }
                /////////////////////////////////////////////////////////////////////
                try
                {
                    object prob_o_code = 0;
                    for (int row = 0; row < DTViewingS.Tables[6].Rows.Count; row++)
                    {

                        SQL = " SELECT MAX(CODE)+1 FROM QA_PROB_COST ";
                        prob_o_code = oracleacess.ExecuteScalar(SQL);
                        if (prob_o_code == DBNull.Value)
                        {
                            prob_o_code = 1;
                        }
                        //SQL = " SELECT MAX(CODE) FROM QA_RESP_PROB ";
                        //object prob_code = oracleacess.ExecuteScalar(SQL);
                        SQL = null;
                        SQL += "insert into QA_PROB_COST (CODE , PROB_CODE , ITEM_NAME, QTY ,PRICE )VALUES (";
                        SQL += prob_o_code.ToString() + "," + prob_code + ",";
                        SQL += "MY_Reverse('" + DTViewingS.Tables[6].Rows[row]["ITEM_NAME"].ToString() + "'),";
                        SQL += DTViewingS.Tables[6].Rows[row]["qty"].ToString() + ",";
                        SQL += DTViewingS.Tables[6].Rows[row]["price"].ToString() + ")";
                        oracleacess.ExecuteNonQuery(SQL);
                    }
                }

                catch (Exception ex)
                {
                    LogError(ex.Message, "upd_prob_items2_new");
                    return false;
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "update_prob_solv_new");
            return false;
        }
    }

    /// <summary>
    /// To Update the day plan
    /// </summary>
    /// <param name="MyDTViewing"></param>
    //[WebMethod]
    public void UPdateViewing(DataSet TimePlanDTViewing)
    {
        try
        {
            for (int row = 0; row < TimePlanDTViewing.Tables[0].Rows.Count; row++)
            {
                SQL = " Update EG_VIEWING SET";
                if (TimePlanDTViewing.Tables[0].Rows[row]["FROM_TIME"] != DBNull.Value)
                {
                    SQL += " FROM_TIME=to_date('" + (Convert.ToDateTime(TimePlanDTViewing.Tables[0].Rows[row]["FROM_TIME"]).ToString("MM/dd/yyyy HH:mm:ss"))
                        + "', 'MM/DD/YYYY HH24:MI:SS') ";
                    //LogError("test Date FROM_TIME", Convert.ToDateTime(TimePlanDTViewing.Tables[0].Rows[row]["FROM_TIME"]).ToString("MM/dd/yyyy HH:mm:ss"));
                }
                if (TimePlanDTViewing.Tables[0].Rows[row]["TO_TIME"] != DBNull.Value)
                {
                    SQL += ", TO_TIME=to_date('" + (Convert.ToDateTime(TimePlanDTViewing.Tables[0].Rows[row]["TO_TIME"]).ToString("MM/dd/yyyy HH:mm:ss"))
                        + "', 'MM/DD/YYYY HH24:MI:SS') " + " ,";
                    //LogError("test Date TO_TIME", Convert.ToDateTime(TimePlanDTViewing.Tables[0].Rows[row]["TO_TIME"]).ToString("MM/dd/yyyy HH:mm:ss"));
                }
                SQL += " PDA_Status=2";// +Convert.ToInt32(TimePlanDTViewing.Tables[0].Rows[row]["PDA_Statuse"].ToString()) + "";
                //SQL += " ,Rep_Reject_Reason='" + TimePlanDTViewing.Tables[0].Rows[row]["Rep_Reject_Reason"].ToString() + "'";
                SQL += " Where CODE=" + TimePlanDTViewing.Tables[0].Rows[row]["CODE"] + "";
                //LogError("test Date TO_TIME", SQL);

                oracleacess.ExecuteNonQuery(SQL);
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateViewing");
        }
    }

    /// <summary>
    /// Update CRM EG_VIEWING Table
    /// </summary>
    /// <param name="DTViewing"></param>
    /// <returns></returns>
    //[WebMethod]
    public int UPdateViewings(DataSet DTViewingS) //public bool UPdateViewings()
    {

        #region EG_view_steps
        try
        {
            for (int r = 0; r < DTViewingS.Tables[1].Rows.Count; r++)
            {
                SQL = "delete from EG_View_Steps where view_code = " + DTViewingS.Tables[1].Rows[r]["View_Code"];
                oracleacess.ExecuteNonQuery(SQL);
            }
            for (int row2 = 0; row2 < DTViewingS.Tables[1].Rows.Count; row2++)
            {
                SQL = " Insert INTO EG_View_Steps(View_Code, step_NO, VIEW_SERIAL, Step_Time, AVLFlag)values("
                    + Convert.ToInt32(DTViewingS.Tables[1].Rows[row2]["View_Code"].ToString()) + "";
                SQL += "," + Convert.ToInt32(DTViewingS.Tables[1].Rows[row2]["step_NO"].ToString()) + "";
                SQL += "," + Convert.ToInt32(DTViewingS.Tables[1].Rows[row2]["PRN_NO"].ToString()) + "";
                SQL += ",to_date('" + (Convert.ToDateTime(DTViewingS.Tables[1].Rows[row2]["Step_Time"]).ToString("MM/dd/yyyy HH:mm:ss"))
                    + "', 'MM/DD/YYYY HH24:MI:SS')" + "";
                //LogError("1- Test UPdateEG_View_Steps", (Convert.ToDateTime(DTViewingEG_View_Steps.Tables[0].Rows[row2]["Step_Time"]).ToString("MM/dd/yyyy HH:mm:ss")));
                //LogError("2- Test UPdateEG_View_Steps", (Convert.ToDateTime(DTViewingEG_View_Steps.Tables[0].Rows[row2]["Step_Time"]).ToString("MM/dd/yyyy HH:mm:ss")));
                SQL += "," + Convert.ToInt32(DTViewingS.Tables[1].Rows[row2]["AVLFlag"].ToString()) + " )";
                //LogError("UPdateEG_View_Steps", SQL);
                oracleacess.ExecuteNonQuery(SQL);
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateEG_View_Steps_new");
            return -1;
        }
        #endregion EG_view_steps
        ////////////////////////////////////////////////////////////////////////////////
        #region EG_viewing_Det
        try
        {
            object det_code = 0;
            for (int r = 0; r < DTViewingS.Tables[2].Rows.Count; r++)
            {
                SQL = "delete from EG_VIEWING_DET where view_code = " + DTViewingS.Tables[2].Rows[r]["View_Code"];
                oracleacess.ExecuteNonQuery(SQL);
            }
            for (int row1 = 0; row1 < DTViewingS.Tables[2].Rows.Count; row1++)
            {
                //SQL = " Select max(CODE)+1 FROM EG_VIEWING_DET";
                //code = oracleacess.ExecuteScalar(SQL);

                SQL = " Insert INTO EG_VIEWING_DET(CODE, VIEW_CODE, ITEM_CODE, VIEW_SERIAL, QTY, POINTS, TOTAL_POINTS)Values("
                     + "View_DET_SEQ.NextVal, "
                    //+ Convert.ToInt32(code.ToString()) + ","
                     + Convert.ToInt32(DTViewingS.Tables[2].Rows[row1]["VIEW_CODE"].ToString())
                     + "," + Convert.ToInt32(DTViewingS.Tables[2].Rows[row1]["ITEM_CODE"].ToString())
                     + "," + Convert.ToInt32(DTViewingS.Tables[2].Rows[row1]["PRN_NO"].ToString())
                     + "," + Convert.ToDouble(DTViewingS.Tables[2].Rows[row1]["QTY"].ToString()) + ""
                     + "," + Convert.ToDouble(DTViewingS.Tables[2].Rows[row1]["POINTS"].ToString()) + ","
                     + (Convert.ToDouble(DTViewingS.Tables[2].Rows[row1]["POINTS"].ToString())
                     * Convert.ToDouble(DTViewingS.Tables[2].Rows[row1]["QTY"].ToString())) + ")";
                oracleacess.ExecuteNonQuery(SQL);
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateViewingDTL_new");
            return -1;
        }
        #endregion EG_viewing_Det
        ///////////////////////////////////////////////////////////////////////////////////
        #region EG_VIEWING
        try
        {
            //DataSet DTViewing = new DataSet();
            //int row = 0;  
            object vcode = 0;
            for (int row = 0; row < DTViewingS.Tables[0].Rows.Count; row++)
            {
                #region EG_Customers
                //if (Convert.ToBoolean(DTViewing.Tables[0].Rows[row]["FlageID"]) == true)
                //{
                //    SQL = "SELECT max(CODE)+1 FROM EG_Customers";
                //    code = oracleacess.ExecuteScalar(SQL);
                //    if (code == DBNull.Value)
                //    {
                //        code = 1;
                //    }
                //    SQL = " Insert INTO EG_Customers(CODE, NAME, TEL, MOBILE, FLAG, ID_NO)VALUES(";
                //    SQL += "" + Convert.ToInt32(code.ToString()) + "";
                //    SQL += ",MY_Reverse('" + DTViewing.Tables[0].Rows[row]["CustName"].ToString() + "')";
                //    SQL += ",'" + DTViewing.Tables[0].Rows[row]["CustTel"].ToString() + "'";
                //    SQL += ",'" + DTViewing.Tables[0].Rows[row]["CustMobile"].ToString() + "'";
                //    SQL += ",1";
                //    SQL += ", " + DTViewing.Tables[0].Rows[row]["ID_NO"].ToString() + ")";
                //    oracleacess.ExecuteNonQuery(SQL);
                //}
                //else
                //{
                //    SQL = " UPDATE EG_Customers SET "
                //      + " NAME =MY_Reverse('" + DTViewing.Tables[0].Rows[row]["CustName"].ToString()
                //      + "'), TEL ='" + DTViewing.Tables[0].Rows[row]["CustTel"].ToString()
                //      + "', MOBILE ='" + DTViewing.Tables[0].Rows[row]["CustMobile"].ToString() + "'";
                //    if (DTViewing.Tables[0].Rows[row]["ID_NO"].ToString() != "")
                //    {
                //        SQL += ", ID_NO  =" + DTViewing.Tables[0].Rows[row]["ID_NO"].ToString();
                //    }
                //    SQL += " WHERE CODE=" + Convert.ToInt32(DTViewing.Tables[0].Rows[row]["CUST_CODE"].ToString())
                //    + " AND (CODE!=8397 AND CODE!=68035 AND CODE!= 65429)";
                //    oracleacess.ExecuteNonQuery(SQL);
                //}
                #endregion
                ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                //#region EG_Viewing
                //EG_VIEWING.GROUP_mang_code
                if (Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["PRN_NO"]) == 1)
                {
                    SQL = "";
                    if (int.Parse(DTViewingS.Tables[0].Rows[row]["GUR_STATUS"].ToString()) == 1)
                    {
                        SQL = " Update EG_VIEWING SET GUR_STATUS='Y'";
                    }
                    else if (int.Parse(DTViewingS.Tables[0].Rows[row]["GUR_STATUS"].ToString()) == 0)
                    {
                        SQL = " Update EG_VIEWING SET GUR_STATUS='N'";
                    }
                    SQL += " ,GUR_REASON=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["GUR_REASON"].ToString() + "')";
                    if (DTViewingS.Tables[0].Rows[row]["POINTS"] == DBNull.Value)
                    {
                        SQL += " ,POINTS=0";
                    }
                    else
                    {
                        SQL += " ,POINTS = " + Convert.ToDouble(DTViewingS.Tables[0].Rows[row]["POINTS"]).ToString() + " ";
                    }

                    SQL += " ,V_DESC=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["V_DESC"].ToString() + "')";
                    SQL += " ,CANCEL_REASON=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["VIEW_Reason"].ToString() + "')";
                    if (DTViewingS.Tables[0].Rows[row]["Level"] == DBNull.Value)
                    {
                        SQL += " ,View_level=0";
                    }
                    else
                    {
                        SQL += " ,View_level=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["Level"]).ToString() + "";
                    }
                    SQL += " ,PDA_Status = 3";
                    SQL += " ,ADDRESS=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["ADDRESS"] + "')";
                    SQL += " ,OWNER_NAME=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["OWNER_NAME"] + "')";
                    SQL += " ,ORDER_STATUS='" + DTViewingS.Tables[0].Rows[row]["VIEW_STATUS"] + "'";
                    SQL += " ,TEL='" + DTViewingS.Tables[0].Rows[row]["TEL"] + "'";
                    SQL += " ,MOB='" + DTViewingS.Tables[0].Rows[row]["MOB"] + "'";
                    if (DTViewingS.Tables[0].Rows[row]["SHOP_CODE"] == DBNull.Value)
                    {
                        SQL += " ,SHOP_CODE=0 ";
                    }
                    else
                    {
                        SQL += " ,SHOP_CODE=" + DTViewingS.Tables[0].Rows[row]["SHOP_CODE"] + "";
                    }
                    SQL += " ,TITLE=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["TITLE"] + "')";

                    SQL += " ,De_NOTES=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["De_NOTE"] + "')";
                    SQL += " ,NOTES=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["NOTES"] + "')";
                    SQL += " ,Shop_Note=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["Shop_Note"] + "')";

                    SQL += " ,Until_Work_Comp=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["Until_Work_Comp"] + "')";
                    if (Convert.ToBoolean(DTViewingS.Tables[0].Rows[row]["FlageID"]) == true)
                    {
                        SQL += " ,CUST_CODE=" + Convert.ToInt32(vcode.ToString()) + "";
                        SQL += ",FLAG = 1";
                    }
                    else
                    {
                        SQL += " ,CUST_CODE=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["CUST_CODE"]).ToString() + "";
                    }
                    SQL += " ,PRN_No=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["CODE"]).ToString() + "";

                    // Update in 20-12-2011 New Request by Assmaa & Essam & Ramy 
                    //, STOP_CERT, STOP_RSN, WC_CNT, KITCHEN_CNT, VIEW_TYPE

                    //STOP_CERT
                    //LogError("1 Test Eslam STOP_CERT",(DTViewing.Tables[0].Rows[row]["STOP_CERT"].ToString()));
                    if (DTViewingS.Tables[0].Rows[row]["STOP_CERT"].ToString() == bool.TrueString)
                    {
                        SQL += " , STOP_CERT='Y'";
                    }
                    else if (DTViewingS.Tables[0].Rows[row]["STOP_CERT"].ToString() == bool.FalseString)
                    {
                        SQL += " , STOP_CERT='N'";
                    }

                    //STOP_RSN
                    //LogError("2 Test Eslam STOP_RSN", (DTViewing.Tables[0].Rows[row]["STOP_RSN"].ToString()));
                    SQL += " , STOP_RSN=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["STOP_RSN"].ToString() + "')";

                    //WC_CNT
                    //LogError("3 Test Eslam WC_CNT", (DTViewing.Tables[0].Rows[row]["WC_CNT"].ToString()));
                    SQL += " , WC_CNT=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["WC_CNT"]).ToString() + "";

                    //KITCHEN_CNT
                    //LogError("4 Test Eslam KITCHEN_CNT", (DTViewing.Tables[0].Rows[row]["KITCHEN_CNT"].ToString()));
                    SQL += " ,KITCHEN_CNT=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["KITCHEN_CNT"]).ToString() + "";

                    //VIEW_TYPE
                    //LogError("5 Test Eslam VIEW_TYPE", (DTViewing.Tables[0].Rows[row]["VIEW_TYPE"].ToString()));
                    SQL += " , VIEW_TYPE=MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["VIEW_TYPE"].ToString() + "')";
                    //End Update in 20-12-2011 New Request by Assmaa & Essam & Ramy 
                    //SQL += " , SEND_TIME='" + DTViewingS.Tables[0].Rows[row]["SEND_TIME"] + "'";
                    SQL += " , SEND_TIME=to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"))
                                        + "','MM/DD/YYYY HH24:MI:SS') ";

                    SQL += " Where CODE=" + DTViewingS.Tables[0].Rows[row]["CODE"] + "";
                    SQL += " And View_Serial=" + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["PRN_No"]).ToString() + "";
                    oracleacess.ExecuteNonQuery(SQL);
                }
                else
                {
                    SQL = " Insert INTO EG_VIEWING(";
                    SQL += "  CODE, PRN_No, CUST_CODE, FLAG, ADDRESS, PLN_DATE, FROM_TIME, TO_TIME";
                    SQL += ", OWNER_NAME, TEL, MOB";
                    SQL += ", FEEDBACK, VIEW_SERIAL, GUR_STATUS, GUR_REASON, POINTS, V_DESC, CANCEL_REASON, View_level, PDA_Status";
                    SQL += ", ORDER_STATUS, SHOP_CODE, TITLE, De_NOTES, NOTES, Shop_Note, Until_Work_Comp, Rep_Code, GROUP_mang_code, ";
                    SQL += "GOV_CODE, CITY_CODE, DIST_CODE ";

                    // Update in 20-12-2011 New Request by Assmaa & Essam & Ramy
                    SQL += ",STOP_CERT, STOP_RSN, WC_CNT, KITCHEN_CNT, VIEW_TYPE , SEND_TIME";
                    //End Update in 20-12-2011 New Request by Assmaa & Essam & Ramy 

                    SQL += " )";
                    SQL += " VALUES(";
                    SQL += "  " + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["CODE"].ToString()) + "";
                    SQL += ",  " + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["CODE"].ToString()) + "";
                    if (Convert.ToBoolean(DTViewingS.Tables[0].Rows[row]["FlageID"]) == true)
                    {
                        SQL += ", " + Convert.ToInt32(vcode.ToString()) + "";
                        SQL += ",1";
                    }
                    else
                    {
                        SQL += ", " + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["CUST_CODE"].ToString()) + "";
                        SQL += ",0";
                    }
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["ADDRESS"].ToString() + "')";
                    SQL += ",to_date('" + (Convert.ToDateTime(DTViewingS.Tables[0].Rows[row]["PLN_DATE"]).ToString("MM/dd/yyyy HH:mm:ss"))
                                        + "','MM/DD/YYYY HH24:MI:SS') ";
                    SQL += ",to_date('" + (Convert.ToDateTime(DTViewingS.Tables[0].Rows[row]["FROM_TIME"]).ToString("MM/dd/yyyy HH:mm:ss"))
                                        + "','MM/DD/YYYY HH24:MI:SS') ";
                    SQL += ",to_date('" + (Convert.ToDateTime(DTViewingS.Tables[0].Rows[row]["TO_TIME"]).ToString("MM/dd/yyyy HH:mm:ss"))
                                        + "','MM/DD/YYYY HH24:MI:SS') ";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["OWNER_NAME"].ToString() + "')";
                    SQL += ",'" + DTViewingS.Tables[0].Rows[row]["TEL"].ToString() + "'";
                    SQL += ",'" + DTViewingS.Tables[0].Rows[row]["MOB"].ToString() + "'";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["NOTES"].ToString() + "')";
                    SQL += ",'" + DTViewingS.Tables[0].Rows[row]["PRN_NO"].ToString() + "'";
                    if (int.Parse(DTViewingS.Tables[0].Rows[row]["GUR_STATUS"].ToString()) == 1)
                    {
                        SQL += " ,'Y'";
                    }
                    else if (int.Parse(DTViewingS.Tables[0].Rows[row]["GUR_STATUS"].ToString()) == 0)
                    {
                        SQL += " ,'N'";
                    }
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["GUR_REASON"].ToString() + "')";
                    if (DTViewingS.Tables[0].Rows[row]["POINTS"] == DBNull.Value)
                    {
                        SQL += ",0";
                    }
                    else
                    {
                        SQL += "," + Convert.ToDouble(DTViewingS.Tables[0].Rows[row]["POINTS"]).ToString() + "";
                    }
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["V_DESC"].ToString() + "')";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["VIEW_Reason"].ToString() + "')";
                    if (DTViewingS.Tables[0].Rows[row]["Level"] == DBNull.Value)
                    {
                        SQL += ",0";
                    }
                    else
                    {
                        SQL += "," + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["Level"]).ToString() + "";
                    }
                    SQL += ",3";
                    SQL += " , '" + DTViewingS.Tables[0].Rows[row]["VIEW_STATUS"] + "'";
                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["SHOP_CODE"] + "";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["TITLE"] + "')";

                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["De_NOTE"] + "')";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["NOTES"] + "')";
                    SQL += ",MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["Shop_Note"] + "')";

                    SQL += ",'" + DTViewingS.Tables[0].Rows[row]["Until_Work_Comp"] + "'";
                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["RepCode"] + "";
                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["GROUP_mang_code"] + "";

                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["GOV_CODE"] + "";
                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["CITY_CODE"] + "";
                    SQL += ", " + DTViewingS.Tables[0].Rows[row]["DIST_CODE"] + "";

                    // Update in 20-12-2011 New Request by Assmaa & Essam & Ramy 
                    //, STOP_CERT, STOP_RSN, WC_CNT, KITCHEN_CNT, VIEW_TYPE                     
                    //STOP_CERT
                    if (int.Parse(DTViewingS.Tables[0].Rows[row]["STOP_CERT"].ToString()) == 1)
                    {
                        SQL += " ,'Y'";
                    }
                    else if (int.Parse(DTViewingS.Tables[0].Rows[row]["STOP_CERT"].ToString()) == 0)
                    {
                        SQL += " ,'N'";
                    }
                    //STOP_RSN
                    SQL += " ,MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["STOP_RSN"] + "')";
                    //WC_CNT
                    SQL += " , " + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["WC_CNT"]).ToString() + "";
                    //KITCHEN_CNT
                    SQL += " , " + Convert.ToInt32(DTViewingS.Tables[0].Rows[row]["KITCHEN_CNT"]).ToString() + "";
                    //VIEW_TYPE
                    SQL += " , MY_Reverse('" + DTViewingS.Tables[0].Rows[row]["VIEW_TYPE"] + "')";
                    //End Update in 20-12-2011 New Request by Assmaa & Essam & Ramy 
                    //SQL += " , '"+ DTViewingS.Tables[0].Rows[row]["SEND_TIME"] +"'";
                    SQL += " , to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"))
                                        + "','MM/DD/YYYY HH24:MI:SS') ";
                    SQL += " )";
                    oracleacess.ExecuteNonQuery(SQL);
                }

                ////////////////////////////////////////////////////////////////////////////////

            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateViewings_new");
            return -1;
        }
        #endregion EG_viewing
        //////////////////////////////////////////////////////////////////////////////
        #region HS
        if (DTViewingS.Tables[3].Rows.Count > 0)
        {
            try
            {
                //int CODE = 0;
                //string USER_NAME = "";
                for (int r = 0; r < DTViewingS.Tables[3].Rows.Count; r++)
                {
                    SQL = "delete from HS_VIEW_PROD where view_code = " + DTViewingS.Tables[3].Rows[r]["View_Code"];
                    oracleacess.ExecuteNonQuery(SQL);
                }
                for (int row2 = 0; row2 < DTViewingS.Tables[3].Rows.Count; row2++)
                {
                    object hs_code = 0;
                    SQL = " SELECT MAX(CODE)+1 FROM HS_VIEW_PROD ";
                    hs_code = oracleacess.ExecuteScalar(SQL);

                    if (hs_code == DBNull.Value)
                    {
                        hs_code = 1;
                    }

                    SQL = null;
                    SQL += " INSERT INTO HS_VIEW_PROD (CODE, GROUP_CODE, PROD_CODE, DIM_CODE, VIEW_CODE, VIEW_SERIAL, PROD_CNT,tot,tot_val)";
                    SQL += " VALUES (";

                    SQL += "" + hs_code.ToString() + "";
                    SQL += "," + int.Parse(DTViewingS.Tables[3].Rows[row2]["GROUP_CODE"].ToString()) + "";
                    SQL += "," + DTViewingS.Tables[3].Rows[row2]["PROD_CODE"].ToString() + "";
                    SQL += "," + DTViewingS.Tables[3].Rows[row2]["DIM_CODE"].ToString() + "";
                    SQL += "," + DTViewingS.Tables[3].Rows[row2]["VIEW_CODE"].ToString() + "";
                    SQL += "," + DTViewingS.Tables[3].Rows[row2]["VIEW_SERIAL"].ToString() + "";

                    SQL += "," + DTViewingS.Tables[3].Rows[row2][6].ToString() + "";
                    SQL += "," + DTViewingS.Tables[3].Rows[row2][7].ToString() + "";//total points
                    SQL += "," + DTViewingS.Tables[3].Rows[row2][8].ToString() + "";//total price
                    SQL += ")";
                    oracleacess.ExecuteNonQuery(SQL);
                    //SQL = null;
                }

            }
            catch (Exception ex)
            {
                LogError(ex.Message, "UPdate_HS_VIEW_PROD_new");
                return -1;
            }
        }
        #endregion HS
        /////////////////////////////////////////////////////////////////////////////////
        #region Problems
        if (DTViewingS.Tables[4].Rows.Count > 0)
        {
            try
            {
                object prob_code = 0;
                for (int r = 0; r < DTViewingS.Tables[4].Rows.Count; r++)
                {
                    SQL = "delete from QA_RESP_PROB where view_code = " + DTViewingS.Tables[4].Rows[r]["View_Code"];
                    oracleacess.ExecuteNonQuery(SQL);
                }
                for (int row = 0; row < DTViewingS.Tables[4].Rows.Count; row++)
                {

                    SQL = " SELECT MAX(CODE)+1 FROM QA_RESP_PROB ";
                    prob_code = oracleacess.ExecuteScalar(SQL);
                    if (prob_code == DBNull.Value)
                    {
                        prob_code = 1;
                    }
                    SQL = null;
                    SQL += "insert into QA_RESP_PROB (CODE , VIEW_CODE , PROB_TEAM_CODE, PROB_HELP ,PROB_KIND,PROB_TYPE,CUST_CODE,CUST_MOBILE,";
                    SQL += "OWNER,OWNER_MOBILE,ADDRESS,PLN_DATE,INIT_VAL ) VALUES (";
                    SQL += prob_code.ToString() + ",";
                    SQL += DTViewingS.Tables[4].Rows[row]["VIEW_CODE"].ToString() + ",";
                    SQL += DTViewingS.Tables[4].Rows[row]["RESP_CODE"].ToString() + ",";
                    SQL += DTViewingS.Tables[4].Rows[row]["PROB_HELP"].ToString() + ",";
                    SQL += "MY_Reverse('" + DTViewingS.Tables[4].Rows[row]["PROB_KIND"].ToString() + "'),";
                    SQL += "MY_Reverse('" + DTViewingS.Tables[4].Rows[row]["PROB_DESC"].ToString() + "'),";
                    SQL += DTViewingS.Tables[4].Rows[row]["CUST_CODE"].ToString() + ",'";
                    SQL += DTViewingS.Tables[4].Rows[row]["CUST_MOB"].ToString() + "',";
                    SQL += "MY_Reverse('" + DTViewingS.Tables[4].Rows[row]["OWNER_NAME"].ToString() + "'),'";
                    SQL += DTViewingS.Tables[4].Rows[row]["OWNER_MOB"].ToString() + "',";
                    SQL += "MY_Reverse('" + DTViewingS.Tables[4].Rows[row]["OWNER_ADDRESS"].ToString() + "'),";
                    SQL += "to_date('" + (Convert.ToDateTime(DTViewingS.Tables[4].Rows[row]["PROB_DATE"]).ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS'),";
                    SQL += DTViewingS.Tables[4].Rows[row]["TOT_COST"] + ")";
                    oracleacess.ExecuteNonQuery(SQL);
                    SQL = "delete from QA_PROB_ITEMS where prob_code = " + prob_code;
                    oracleacess.ExecuteNonQuery(SQL);
                    SQL = "delete from QA_PROB_COST where prob_code = " + prob_code;
                    oracleacess.ExecuteNonQuery(SQL);
                }

                ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                if (DTViewingS.Tables.Count > 4)
                {
                    object prob_c_code = 0;
                    try
                    {

                        for (int row = 0; row < DTViewingS.Tables[5].Rows.Count; row++)
                        {

                            SQL = " SELECT MAX(CODE)+1 FROM QA_PROB_ITEMS ";
                            prob_c_code = oracleacess.ExecuteScalar(SQL);
                            if (prob_c_code == DBNull.Value)
                            {
                                prob_c_code = 1;
                            }
                            //SQL = " SELECT MAX(CODE) FROM QA_RESP_PROB ";
                            //object prob_code = oracleacess.ExecuteScalar(SQL);
                            SQL = null;
                            SQL += "insert into QA_PROB_ITEMS (CODE , PROB_CODE , ITEM_CODE ,QTY,price,tot )VALUES (";
                            SQL += prob_c_code.ToString() + "," + prob_code + ",";
                            SQL += DTViewingS.Tables[5].Rows[row]["ITEM_CODE"].ToString() + ",";
                            SQL += DTViewingS.Tables[5].Rows[row]["qty"].ToString() + ",";
                            SQL += DTViewingS.Tables[5].Rows[row]["price"].ToString() + ",";
                            SQL += DTViewingS.Tables[5].Rows[row]["tot"].ToString();
                            SQL += ")";
                            oracleacess.ExecuteNonQuery(SQL);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogError(ex.Message, "upd_prob_items_new");
                        return -1;
                    }
                    /////////////////////////////////////////////////////////////////////
                    try
                    {
                        object prob_o_code = 0;
                        for (int row = 0; row < DTViewingS.Tables[6].Rows.Count; row++)
                        {

                            SQL = " SELECT MAX(CODE)+1 FROM QA_PROB_COST ";
                            prob_o_code = oracleacess.ExecuteScalar(SQL);
                            if (prob_o_code == DBNull.Value)
                            {
                                prob_o_code = 1;
                            }
                            //SQL = " SELECT MAX(CODE) FROM QA_RESP_PROB ";
                            //object prob_code = oracleacess.ExecuteScalar(SQL);
                            SQL = null;
                            SQL += "insert into QA_PROB_COST (CODE , PROB_CODE , ITEM_NAME, QTY ,PRICE )VALUES (";
                            SQL += prob_o_code.ToString() + "," + prob_code + ",";
                            SQL += "MY_Reverse('" + DTViewingS.Tables[6].Rows[row]["ITEM_NAME"].ToString() + "'),";
                            SQL += DTViewingS.Tables[6].Rows[row]["qty"].ToString() + ",";
                            SQL += DTViewingS.Tables[6].Rows[row]["price"].ToString() + ")";
                            oracleacess.ExecuteNonQuery(SQL);
                        }
                    }

                    catch (Exception ex)
                    {
                        LogError(ex.Message, "upd_prob_items2_new");
                        return -1;
                    }
                }
            }
            catch (Exception ex)
            {
                LogError(ex.Message, "update_prob_solv_new");
                return -1;
            }
        }
        #endregion Problems
        return DTViewingS.Tables[0].Rows.Count;
    }

    /// <summary>
    /// Update CRM EG_VIEWING_DET Table
    /// </summary>
    /// <param name="DTViewing"></param>
    /// <returns></returns>
    //[WebMethod]
    public bool UPdateViewingDTL(DataSet DTViewingDTL)
    {
        try
        {
            object code = 0;
            for (int row1 = 0; row1 < DTViewingDTL.Tables[0].Rows.Count; row1++)
            {
                //SQL = " Select max(CODE)+1 FROM EG_VIEWING_DET";
                //code = oracleacess.ExecuteScalar(SQL);

                SQL = " Insert INTO EG_VIEWING_DET(CODE, VIEW_CODE, ITEM_CODE, VIEW_SERIAL, QTY, POINTS, TOTAL_POINTS)Values("
                     + "View_DET_SEQ.NextVal, "
                    //+ Convert.ToInt32(code.ToString()) + ","
                     + Convert.ToInt32(DTViewingDTL.Tables[0].Rows[row1]["VIEW_CODE"].ToString())
                     + "," + Convert.ToInt32(DTViewingDTL.Tables[0].Rows[row1]["ITEM_CODE"].ToString())
                     + "," + Convert.ToInt32(DTViewingDTL.Tables[0].Rows[row1]["PRN_NO"].ToString())
                     + "," + Convert.ToDouble(DTViewingDTL.Tables[0].Rows[row1]["QTY"].ToString()) + ""
                     + "," + Convert.ToDouble(DTViewingDTL.Tables[0].Rows[row1]["POINTS"].ToString()) + ","
                     + (Convert.ToDouble(DTViewingDTL.Tables[0].Rows[row1]["POINTS"].ToString())
                     * Convert.ToDouble(DTViewingDTL.Tables[0].Rows[row1]["QTY"].ToString())) + ")";
                oracleacess.ExecuteNonQuery(SQL);
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateViewingDTL");
            return false;
        }
    }

    /// <summary>
    /// Update CRM EG_View_Steps Table
    /// </summary>
    /// <param name="DTViewing"></param>
    /// <returns></returns>
    //[WebMethod]
    public bool UPdateEG_View_Steps(DataSet DTViewingEG_View_Steps)
    {
        try
        {
            for (int row2 = 0; row2 < DTViewingEG_View_Steps.Tables[0].Rows.Count; row2++)
            {
                SQL = " Insert INTO EG_View_Steps(View_Code, step_NO, VIEW_SERIAL, Step_Time, AVLFlag)values("
                    + Convert.ToInt32(DTViewingEG_View_Steps.Tables[0].Rows[row2]["View_Code"].ToString()) + "";
                SQL += "," + Convert.ToInt32(DTViewingEG_View_Steps.Tables[0].Rows[row2]["step_NO"].ToString()) + "";
                SQL += "," + Convert.ToInt32(DTViewingEG_View_Steps.Tables[0].Rows[row2]["PRN_NO"].ToString()) + "";
                SQL += ",to_date('" + (Convert.ToDateTime(DTViewingEG_View_Steps.Tables[0].Rows[row2]["Step_Time"]).ToString("MM/dd/yyyy HH:mm:ss"))
                    + "', 'MM/DD/YYYY HH24:MI:SS')" + "";
                //LogError("1- Test UPdateEG_View_Steps", (Convert.ToDateTime(DTViewingEG_View_Steps.Tables[0].Rows[row2]["Step_Time"]).ToString("MM/dd/yyyy HH:mm:ss")));
                //LogError("2- Test UPdateEG_View_Steps", (Convert.ToDateTime(DTViewingEG_View_Steps.Tables[0].Rows[row2]["Step_Time"]).ToString("MM/dd/yyyy HH:mm:ss")));
                SQL += "," + Convert.ToInt32(DTViewingEG_View_Steps.Tables[0].Rows[row2]["AVLFlag"].ToString()) + " )";
                //LogError("UPdateEG_View_Steps", SQL);
                oracleacess.ExecuteNonQuery(SQL);
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateEG_View_Steps");
            return false;
        }
    }

    /// <summary>
    /// Update CRM PrizeCard Table
    /// </summary>
    /// <param name="DTViewing"></param>
    /// <returns></returns>
    //[WebMethod]
    public bool UPdatePrizeCard(DataSet DTViewingPrizeCard)
    {
        try
        {
            object code = 0;
            for (int row2 = 0; row2 < DTViewingPrizeCard.Tables[0].Rows.Count; row2++)
            {
                SQL = " SELECT MAX(CODE)+1 FROM EG_VIEW_POINTS";
                code = oracleacess.ExecuteScalar(SQL);

                if (code == DBNull.Value)
                {
                    code = 1;
                }

                SQL = " INSERT INTO EG_VIEW_POINTS(CODE, VIEW_CODE, VIEW_SERIAL, POINTS_CODE, PRIZE)";
                SQL += "  VALUES(" + Convert.ToInt32(code.ToString()) + "," + Convert.ToInt32(DTViewingPrizeCard.Tables[0].Rows[row2]["VIEW_CODE"].ToString()) + " ," + Convert.ToInt32(DTViewingPrizeCard.Tables[0].Rows[row2]["PRN_NO"].ToString()) + ",3,'" + DTViewingPrizeCard.Tables[0].Rows[row2]["NAME"].ToString() + "')";
                oracleacess.ExecuteNonQuery(SQL);
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdatePrizeCard");
            return false;
        }
    }

    /// <summary>
    /// Update CRM JOC_TRNS Table
    /// </summary>
    /// <param name="DTViewing"></param>
    /// <returns></returns>
    //[WebMethod]
    public bool UPdateJOC_TRNS(DataSet DTViewingJOC_TRNS)
    {
        try
        {
            for (int row2 = 0; row2 < DTViewingJOC_TRNS.Tables[0].Rows.Count; row2++)
            {
                if (Convert.ToInt32(DTViewingJOC_TRNS.Tables[0].Rows[row2]["Type"].ToString()) == 0)
                {
                    SQL = " Insert INTO JOC_TRNS(BK_NO, SHOP_CODE, BRANCH, VIEW_CODE, JCode, Prize, PrizeStatus, JType, VIEW_SERIAL, Prize_Code)VALUES("
                            + Convert.ToInt32(DTViewingJOC_TRNS.Tables[0].Rows[row2]["BK_NO"].ToString()) + ","
                            + DTViewingJOC_TRNS.Tables[0].Rows[row2]["SHOP_CODE"].ToString();
                    SQL += ",'" + DTViewingJOC_TRNS.Tables[0].Rows[row2]["BRANCH"].ToString() + "'";
                    SQL += "," + Convert.ToInt32(DTViewingJOC_TRNS.Tables[0].Rows[row2]["VIEW_CODE"].ToString());
                    SQL += "," + Convert.ToInt32(DTViewingJOC_TRNS.Tables[0].Rows[row2]["JCode"].ToString());
                    SQL += ",MY_Reverse('" + DTViewingJOC_TRNS.Tables[0].Rows[row2]["Prize"].ToString() + "')";
                    SQL += ",'" + DTViewingJOC_TRNS.Tables[0].Rows[row2]["PrizeStatus"].ToString() + "'";
                    SQL += ",1";
                    SQL += "," + Convert.ToInt32(DTViewingJOC_TRNS.Tables[0].Rows[row2]["PRN_NO"].ToString());
                    SQL += "," + Convert.ToInt32(DTViewingJOC_TRNS.Tables[0].Rows[row2]["Prize_Code"].ToString());
                    SQL += ")";
                    oracleacess.ExecuteNonQuery(SQL);
                }
                else
                {
                    object code = 0;
                    SQL = " SELECT MAX(CODE)+1 FROM EG_VIEW_POINTS";
                    code = oracleacess.ExecuteScalar(SQL);

                    if (code == DBNull.Value)
                    {
                        code = 1;
                    }

                    SQL = " INSERT INTO EG_VIEW_POINTS(CODE , VIEW_CODE, VIEW_SERIAL, POINTS_CODE)";
                    SQL += "  VALUES(" + Convert.ToInt32(code.ToString()) + "," + Convert.ToInt32(DTViewingJOC_TRNS.Tables[0].Rows[row2]["VIEW_CODE"].ToString()) + " ," + Convert.ToInt32(DTViewingJOC_TRNS.Tables[0].Rows[row2]["PRN_NO"].ToString()) + ",5)";
                    oracleacess.ExecuteNonQuery(SQL);
                }
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateJOC_TRNS");
            return false;
        }
    }

    /// <summary>
    /// Update CRM VIEW_HANDBOOK Table
    /// </summary>
    /// <param name="DTVIEW_HANDBOOK"></param>
    /// <returns></returns>
    //[WebMethod]
    public bool UPdateVIEW_HANDBOOK(DataSet DTVIEW_HANDBOOK)
    {
        try
        {
            for (int row1 = 0; row1 < DTVIEW_HANDBOOK.Tables[0].Rows.Count; row1++)
            {
                SQL = " INSERT INTO VIEW_HANDBOOK (VIEW_CODE, VIEW_SERIAL, IMG_CODE, OPEN_TIME)VALUES ("
                     + Convert.ToInt32(DTVIEW_HANDBOOK.Tables[0].Rows[row1]["VIEW_CODE"].ToString())
                     + "," + Convert.ToInt32(DTVIEW_HANDBOOK.Tables[0].Rows[row1]["VIEW_SERIAL"].ToString())
                     + "," + Convert.ToInt32(DTVIEW_HANDBOOK.Tables[0].Rows[row1]["IMG_CODE"].ToString())
                     + ",to_date('" + (Convert.ToDateTime(DTVIEW_HANDBOOK.Tables[0].Rows[row1]["OPEN_TIME"]).ToString("MM/dd/yyyy HH:mm:ss"))
                     + "','MM/DD/YYYY HH24:MI:SS') "
                + ")";
                oracleacess.ExecuteNonQuery(SQL);
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateVIEW_HANDBOOK");
            return false;
        }
    }

    /// <summary>
    /// Update CRM EG_CALL_TRANS Table
    /// </summary>
    /// <param name="DTEG_CALL_TRANS"></param>
    /// <returns></returns>
    //[WebMethod]
    public bool UPdateEG_CALL_TRANS(DataSet DTEG_CALL_TRANS)
    {
        try
        {
            //int CODE = 0;
            string USER_NAME = "";
            for (int row2 = 0; row2 < DTEG_CALL_TRANS.Tables[0].Rows.Count; row2++)
            {
                object code = 0;
                SQL = " SELECT MAX(CODE)+1 FROM EG_CALL_TRANS ";
                code = oracleacess.ExecuteScalar(SQL);

                if (code == DBNull.Value)
                {
                    code = 1;
                }

                SQL = " SELECT NAME FROM eg_reps er WHERE er.code=" + Convert.ToInt32(DTEG_CALL_TRANS.Tables[0].Rows[row2]["USER_NAME"].ToString());
                USER_NAME = oracleacess.ExecuteScalar(SQL).ToString();

                SQL = null;
                SQL = " INSERT INTO EG_CALL_TRANS (CODE, CALLER_TEL, ";
                SQL += " PROBLEM, USER_NAME, REC_DATE, CALL_FROM, CALLER_TYPE, TRANS_TO ,TRANS_DESC) VALUES(";
                SQL += "" + code.ToString() + "";
                //if (DTEG_CALL_TRANS.Tables[0].Rows[row2]["CUST_CODE"].ToString() == "")
                //{
                //    SQL += "," + "NULL" + "";
                //}
                //else
                //{
                //    SQL += "," + int.Parse(DTEG_CALL_TRANS.Tables[0].Rows[row2]["CUST_CODE"].ToString()) + "";
                //}
                //SQL += ",'" + DTEG_CALL_TRANS.Tables[0].Rows[row2]["STATUS"].ToString() + "'";
                SQL += ",'" + DTEG_CALL_TRANS.Tables[0].Rows[row2]["CALLER_TEL"].ToString() + "'";
                SQL += ",MY_Reverse('" + DTEG_CALL_TRANS.Tables[0].Rows[row2]["PROBLEM"].ToString() + "')";
                SQL += ",'" + USER_NAME.ToString() + "'";
                //SQL += ",to_date('" + (Convert.ToDateTime(DTEG_CALL_TRANS.Tables[0].Rows[row2]["REC_DATE"]).ToString("MM/dd/yyyy HH:mm:ss"))
                //    + "','MM/DD/YYYY HH24:MI:SS') ";
                SQL += ", SYSDATE ";
                SQL += ",MY_Reverse('" + DTEG_CALL_TRANS.Tables[0].Rows[row2]["CALL_FROM"].ToString() + "')";
                //SQL += "," + int.Parse(DTEG_CALL_TRANS.Tables[0].Rows[row2]["ORDER_NO"].ToString()) + "";
                SQL += "," + int.Parse(DTEG_CALL_TRANS.Tables[0].Rows[row2]["CALLER_TYPE"].ToString()) + "";
                SQL += ",MY_Reverse('" + DTEG_CALL_TRANS.Tables[0].Rows[row2]["TRANS_TO"].ToString() + "')";
                SQL += ",MY_Reverse('" + DTEG_CALL_TRANS.Tables[0].Rows[row2]["TRANS_DESC"].ToString() + "')";
                SQL += ")";
                oracleacess.ExecuteNonQuery(SQL);
                //SQL = null;
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdateEG_CALL_TRANS");
            return false;
        }
    }

    #endregion

    //************************** Customer *************************
    //GetCustomer

    #region "Form Technical Report"
    //[WebMethod]
    public DataSet GetBacicCode_Customer(string BRANCH, string ID)
    {
        SQL = " Select Code, NAME, EG_GET_POINTS(Code) AS Points, EG_GET_BAL(Code) AS Bal";
        SQL += " From EG_Customers ";
        SQL += " Where MY_Reverse(BRANCH) ='" + BRANCH + "' AND id_no='" + ID + "'";

        Ds.Tables.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetCustomerAcount(int Year_NO, int custcode)
    {//public DataSet GetCustomerAcount(DateTime fromdate, DateTime todate, int custcode)
        SQL = " select to_char(pln_date,'dd/mm/yyyy') as order_date,order_no,points,owner_name,address,gur_status,gur_reason,certificate_receiver FROM CUST_STATEMENT WHERE Year_NO=" + Year_NO + " AND cust_code=" + custcode + " and points != 0 order by Year_NO";
        //SQL = " SELECT * FROM CUST_STATEMENT WHERE pln_date BETWEEN to_date('" + fromdate + "','MM/dd/yyyy') AND to_date('" + todate + "','MM/dd/yyyy') AND cust_code=" + custcode + " order by pln_date";

        Ds.Tables.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }
    #endregion "Form Technical Report"

    //SetCustome
    //[WebMethod]
    public void SaveCustomer(DataSet ds)
    {
        //myado.SaveData(ds.Tables[0], "select * from EG_Customers");
        object code = 0;
        for (int row = 0; row < ds.Tables[0].Rows.Count; row++)
        {
            SQL = "select max(CODE)+1 from EG_CUSTOMERS_TMP";
            code = oracleacess.ExecuteScalar(SQL);
            if (code == DBNull.Value)
            {
                code = 1;
            }

            SQL = " INSERT INTO EG_CUSTOMERS_TMP(Code, BRANCH, NAME, TEL, MOBILE, GOV_CODE, CITY_CODE, DIST_CODE, ID_NO)";
            SQL += " VALUES(" + code + "  ";
            SQL += ",'" + ds.Tables[0].Rows[row]["BRANCH"] + " '";
            SQL += ",MY_Reverse('" + ds.Tables[0].Rows[row]["NAME"] + "')";
            SQL += ",'" + ds.Tables[0].Rows[row]["TEL"] + " '";
            SQL += ",'" + ds.Tables[0].Rows[row]["MOBILE"] + " '";
            SQL += ", " + ds.Tables[0].Rows[row]["GOV_CODE"];
            SQL += ", " + ds.Tables[0].Rows[row]["CITY_CODE"];
            SQL += ", " + ds.Tables[0].Rows[row]["DIST_CODE"];
            SQL += ", " + ds.Tables[0].Rows[row]["ID_NO"];
            SQL += ")";
            oracleacess.ExecuteNonQuery(SQL);

            //oracleacess.SaveData(ds.Tables[0], "select * from EG_CUSTOMERS_TMP");
        }
    }

    //To get total points of customer
    //[WebMethod]
    public int GetCustPoints(int CustCode)
    {
        int Points;
        SQL = "SELECT SUM(EG_VIEWING_DET.POINTS) AS SumPoints ";
        SQL += " FROM  EG_VIEWING INNER JOIN EG_VIEWING_DET ON EG_VIEWING.CODE = EG_VIEWING_DET.CODE";
        SQL += " WHERE EG_VIEWING.CUST_CODE =" + CustCode;
        object Pno = oracleacess.ExecuteScalar(SQL);
        if (!(Pno == System.DBNull.Value)) { Points = Convert.ToInt32(Pno); } else { Points = 0; }
        return Points;
    }

    /// <summary>
    /// GET VIEW STEPS
    /// </summary>
    /// <returns></returns>
    /// 
    //[WebMethod]
    public DataSet GetView_Steps()
    {
        //VIEW_STEPS
        //SQL = "Select * FROM VIEW_STEPS";
        SQL = " Select step_no, STEP_PROMPT, gur_chk, PRODUCT_GROUP From VIEW_STEPS";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    /// <summary>
    /// GET New VIEW STEPS
    /// </summary>
    /// <returns></returns>
    /// 
    //[WebMethod]
    public DataSet NewGetView_Steps()
    {
        //New VIEW_STEPS
        //SQL = "Select * FROM VIEW_STEPS";
        //SQL = "Select step_no,STEP_PROMPT ,gur_chk,PRODUCT_GROUP from VIEW_STEPS";
        //Ds.Clear();
        //dt = oracleacess.ExecuteDataTable(SQL);
        //Ds.Tables.Add(dt);
        //return Ds;

        //========= New VIEW_STEPS ============
        DataTable Dt1 = new DataTable();
        DataTable Dt2 = new DataTable();
        DataTable Dt3 = new DataTable();
        DataTable Dt4 = new DataTable();
        DataTable Dt5 = new DataTable();

        Dt1.Clear();
        Dt2.Clear();
        Dt3.Clear();
        Dt4.Clear();
        Dt5.Clear();


        Ds.Clear();
        SQL = " SELECT * FROM PP_PROD_GROUPS ";
        Dt1 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt1.Copy());

        SQL = " SELECT * FROM PP_SUB_GROUPS ";
        Dt2 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt2.Copy());

        SQL = " SELECT * FROM PP_PROD_CATEGORY ";
        Dt3 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt3.Copy());

        SQL = " SELECT * FROM PP_PG_STEPS ";
        Dt4 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt4.Copy());

        SQL = " SELECT * FROM PP_CATEGORY_STEPS ";
        Dt5 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt5.Copy());
        return Ds;
    }

    /// <summary>
    /// GET VIEW LEVELS 
    /// </summary>
    /// <returns></returns>
    /// 
    //[WebMethod]
    public DataSet GetLEVELS()
    {
        //VIEW LEVEL MAIN
        SQL = " Select * From VIEW_LEVEL_MAIN";
        Ds.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        // VIEW LEVEL DETAILs
        SQL = " Select * From VIEW_LEVEL_DET";
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetScratch(int RepCode)
    {
        //Scratch
        DataTable Dt1 = new DataTable();
        DataTable Dt2 = new DataTable();
        DataTable Dt3 = new DataTable();
        DataTable Dt4 = new DataTable();
        DataTable Dt5 = new DataTable();
        DataTable Dt6 = new DataTable();
        DataTable Dt7 = new DataTable();

        Dt1.Clear();
        Dt2.Clear();
        Dt3.Clear();
        Dt4.Clear();
        Dt5.Clear();
        Dt6.Clear();
        Dt7.Clear();

        Ds.Clear();
        SQL = " SELECT Distinct SCRATCH.CODE, SCRATCH.NAME,SCRATCH.from_date,SCRATCH.TO_DATE,SCRATCH.SC_DESC, SCRATCH.ALL_BRANCHS, SCRATCH.ALL_CITY, SCRATCH.ALL_GOV, SCRATCH.ALL_DIST,"
            + " SCRATCH.ALL_SHOP, SCRATCH.USER_NAME, SCRATCH.REC_DATE"
            + " FROM SCRATCH INNER JOIN"
            + " REP_GIFTS ON SCRATCH.CODE = REP_GIFTS.SC_CODE"
            + " WHERE Trunc(sysdate) between FROM_DATE AND TO_DATE "
            + " AND LOWER(SCRATCH.ACTIVE) = 'y' AND REP_GIFTS.REP_CODE =" + RepCode;
        Dt1 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt1.Copy());

        SQL = " SELECT REP_GIFTS.SC_CODE, REP_GIFTS.GIFT_CODE, REP_GIFTS.QTY, REP_GIFTS.REP_CODE"
            + " FROM SCRATCH INNER JOIN"
            + " REP_GIFTS ON SCRATCH.CODE = REP_GIFTS.SC_CODE"
            + " WHERE Trunc(sysdate) between FROM_DATE AND TO_DATE "
            + " AND LOWER(SCRATCH.ACTIVE) = 'y' AND REP_GIFTS.REP_CODE =" + RepCode;
        Dt2 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt2.Copy());

        SQL = " SELECT SCRATCH_GOVS.SC_CODE , SCRATCH_GOVS.GOV_CODE "
            + " FROM SCRATCH INNER JOIN "
            + " REP_GIFTS ON SCRATCH.CODE = REP_GIFTS.SC_CODE INNER JOIN"
            + " SCRATCH_GOVS ON SCRATCH.CODE = SCRATCH_GOVS.SC_CODE"
            + " WHERE Trunc(sysdate) between FROM_DATE AND TO_DATE "
            + " AND LOWER(SCRATCH.ACTIVE) = 'y' AND REP_GIFTS.REP_CODE =" + RepCode;
        Dt7 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt7.Copy());

        SQL = " SELECT SCRATCH_CITIES.SC_CODE , SCRATCH_CITIES.CITY_CODE"
            + " FROM SCRATCH INNER JOIN"
            + " REP_GIFTS ON SCRATCH.CODE = REP_GIFTS.SC_CODE INNER JOIN"
            + " SCRATCH_CITIES ON SCRATCH.CODE = SCRATCH_CITIES.SC_CODE"
            + " WHERE Trunc(sysdate) between FROM_DATE AND TO_DATE "
            + " AND LOWER(SCRATCH.ACTIVE) = 'y' AND REP_GIFTS.REP_CODE =" + RepCode;
        Dt3 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt3.Copy());

        SQL = " SELECT SCRATCH_DISTRICTS.SC_CODE, SCRATCH_DISTRICTS.DIST_CODE"
            + " FROM SCRATCH INNER JOIN"
            + " REP_GIFTS ON SCRATCH.CODE = REP_GIFTS.SC_CODE INNER JOIN"
            + " SCRATCH_DISTRICTS ON SCRATCH.CODE = SCRATCH_DISTRICTS.SC_CODE"
            + " WHERE Trunc(sysdate) between FROM_DATE AND TO_DATE "
            + " AND LOWER(SCRATCH.ACTIVE) = 'y' AND REP_GIFTS.REP_CODE =" + RepCode;
        Dt4 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt4.Copy());

        SQL = " SELECT SCRATCH_SHOPS.SC_CODE, SCRATCH_SHOPS.SHOP_CODE"
            + " FROM SCRATCH INNER JOIN"
            + " REP_GIFTS ON SCRATCH.CODE = REP_GIFTS.SC_CODE INNER JOIN"
            + " SCRATCH_SHOPS ON SCRATCH.CODE = SCRATCH_SHOPS.SC_CODE"
            + " WHERE Trunc(sysdate) between FROM_DATE AND TO_DATE "
            + " AND LOWER(SCRATCH.ACTIVE) = 'y' AND REP_GIFTS.REP_CODE =" + RepCode;
        Dt5 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt5.Copy());

        SQL = " SELECT * FROM JOC_GIFTS";
        Dt6 = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(Dt6.Copy());

        return Ds;
    }

    //[WebMethod]
    public DataSet GetHANDBOOK_IMG()
    {
        SQL = " Select * FROM HANDBOOK_IMG";
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetTRANS_TYPES()
    {
        SQL = " Select * FROM TRANS_TYPES";
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }
    //===========================================================================
    /// <summary>
    /// GET Resp Productivity 
    /// </summary>
    /// <returns></returns>
    /// 
    //[WebMethod]
    public DataSet GetRep_Productivity(string m, string y, int code)
    {
        Ds.Clear(); dt.Clear();
        SQL = "select to_char(PLN_DATE,'dd/mm/yyyy') AS التاريخ, EXECUTED as منفذ,DELAYED as مؤجل,CANCELED as مرفوض,POINTS as درجات from REP_PRODUCTIVITY where REP_CODE = " + code
            + " AND TO_NUMBER(TO_CHAR(PLN_DATE,'yyyy'))= '" + y
            + "' and TO_NUMBER(TO_CHAR(PLN_DATE,'mm'))= '" + m + "' order by PLN_DATE";
        dt = oracleacess.ExecuteDataTable(SQL);

        Ds.Tables.Add(dt);
        return Ds;
    }

    #region "HS_PROD"

    //[WebMethod]
    public DataSet GetHS_PROD_GROUP()
    {
        SQL = " Select * From HS_PROD_GROUP ";
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetHS_PROD()
    {
        SQL = " Select * From HS_PROD ";
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public DataSet GetHS_PROD_DIM()
    {
        SQL = " Select * From HS_PROD_DIM ";
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }

    //[WebMethod]
    public bool UPdate_HS_VIEW_PROD(DataSet DT_HS_VIEW_PROD)
    {
        try
        {
            //int CODE = 0;
            //string USER_NAME = "";
            for (int row2 = 0; row2 < DT_HS_VIEW_PROD.Tables[0].Rows.Count; row2++)
            {
                object code = 0;
                SQL = " SELECT MAX(CODE)+1 FROM HS_VIEW_PROD ";
                code = oracleacess.ExecuteScalar(SQL);

                if (code == DBNull.Value)
                {
                    code = 1;
                }

                SQL = null;
                SQL += " INSERT INTO HS_VIEW_PROD (CODE, GROUP_CODE, PROD_CODE, DIM_CODE, VIEW_CODE, VIEW_SERIAL, PROD_CNT)";
                SQL += " VALUES (";

                SQL += "" + code.ToString() + "";
                SQL += "," + int.Parse(DT_HS_VIEW_PROD.Tables[0].Rows[row2]["GROUP_CODE"].ToString()) + "";
                SQL += "," + DT_HS_VIEW_PROD.Tables[0].Rows[row2]["PROD_CODE"].ToString() + "";
                SQL += "," + DT_HS_VIEW_PROD.Tables[0].Rows[row2]["DIM_CODE"].ToString() + "";
                SQL += "," + DT_HS_VIEW_PROD.Tables[0].Rows[row2]["VIEW_CODE"].ToString() + "";
                SQL += "," + DT_HS_VIEW_PROD.Tables[0].Rows[row2]["VIEW_SERIAL"].ToString() + "";

                SQL += "," + DT_HS_VIEW_PROD.Tables[0].Rows[row2][6].ToString() + "";

                SQL += ")";
                oracleacess.ExecuteNonQuery(SQL);
                //SQL = null;
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPdate_HS_VIEW_PROD");
            return false;
        }
    }

    #endregion
    # region Problems
    //[WebMethod]
    public bool update_prob_solv(DataSet ds_prob_solv)
    {
        try
        {

            for (int row = 0; row < ds_prob_solv.Tables[0].Rows.Count; row++)
            {
                object code = 0;
                SQL = " SELECT MAX(CODE)+1 FROM QA_RESP_PROB ";
                code = oracleacess.ExecuteScalar(SQL);
                if (code == DBNull.Value)
                {
                    code = 1;
                }
                SQL = null;
                SQL += "insert into QA_RESP_PROB (CODE , VIEW_CODE , PROB_TEAM_CODE, PROB_HELP ,PROB_KIND,PROB_TYPE,CUST_CODE,CUST_MOBILE,";
                SQL += "OWNER,OWNER_MOBILE,ADDRESS,PLN_DATE ) VALUES (";
                SQL += code.ToString() + ",";
                SQL += ds_prob_solv.Tables[0].Rows[row]["VIEW_CODE"].ToString() + ",";
                SQL += ds_prob_solv.Tables[0].Rows[row]["RESP_CODE"].ToString() + ",";
                SQL += ds_prob_solv.Tables[0].Rows[row]["PROB_HELP"].ToString() + ",";
                SQL += "MY_Reverse('" + ds_prob_solv.Tables[0].Rows[row]["PROB_KIND"].ToString() + "'),";
                SQL += "MY_Reverse('" + ds_prob_solv.Tables[0].Rows[row]["PROB_DESC"].ToString() + "'),";
                SQL += ds_prob_solv.Tables[0].Rows[row]["CUST_CODE"].ToString() + ",'";
                SQL += ds_prob_solv.Tables[0].Rows[row]["CUST_MOB"].ToString() + "',";
                SQL += "MY_Reverse('" + ds_prob_solv.Tables[0].Rows[row]["OWNER_NAME"].ToString() + "'),'";
                SQL += ds_prob_solv.Tables[0].Rows[row]["OWNER_MOB"].ToString() + "',";
                SQL += "MY_Reverse('" + ds_prob_solv.Tables[0].Rows[row]["OWNER_ADDRESS"].ToString() + "'),";
                SQL += "to_date('" + (Convert.ToDateTime(ds_prob_solv.Tables[0].Rows[row]["PROB_DATE"]).ToString("MM/dd/yyyy HH:mm:ss")) + "','MM/DD/YYYY HH24:MI:SS'))";
                oracleacess.ExecuteNonQuery(SQL);
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "update_prob_solv");
            return false;
        }
    }
    //[WebMethod]
    public bool upd_prob_items(DataSet ds_prob_items)
    {
        try
        {

            for (int row = 0; row < ds_prob_items.Tables[0].Rows.Count; row++)
            {
                object code = 0;
                SQL = " SELECT MAX(CODE)+1 FROM QA_PROB_ITEMS ";
                code = oracleacess.ExecuteScalar(SQL);
                if (code == DBNull.Value)
                {
                    code = 1;
                }
                SQL = " SELECT MAX(CODE) FROM QA_RESP_PROB ";
                object prob_code = oracleacess.ExecuteScalar(SQL);
                SQL = null;
                SQL += "insert into QA_PROB_ITEMS (CODE , PROB_CODE , ITEM_CODE ,QTY,price,tot )VALUES (";
                SQL += code.ToString() + "," + prob_code + ",";
                SQL += ds_prob_items.Tables[0].Rows[row]["ITEM_CODE"].ToString() + ",";
                SQL += ds_prob_items.Tables[0].Rows[row]["qty"].ToString() + ",";
                SQL += ds_prob_items.Tables[0].Rows[row]["price"].ToString() + ",";
                SQL += ds_prob_items.Tables[0].Rows[row]["tot"].ToString();
                SQL += ")";
                oracleacess.ExecuteNonQuery(SQL);
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "upd_prob_items");
            return false;
        }
    }
    //[WebMethod]
    public bool upd_prob_items2(DataSet ds_prob_items2)
    {
        try
        {

            for (int row = 0; row < ds_prob_items2.Tables[0].Rows.Count; row++)
            {
                object code = 0;
                SQL = " SELECT MAX(CODE)+1 FROM QA_PROB_COST ";
                code = oracleacess.ExecuteScalar(SQL);
                if (code == DBNull.Value)
                {
                    code = 1;
                }
                SQL = " SELECT MAX(CODE) FROM QA_RESP_PROB ";
                object prob_code = oracleacess.ExecuteScalar(SQL);
                SQL = null;
                SQL += "insert into QA_PROB_COST (CODE , PROB_CODE , ITEM_NAME, QTY ,PRICE )VALUES (";
                SQL += code.ToString() + "," + prob_code + ",";
                SQL += "MY_Reverse('" + ds_prob_items2.Tables[0].Rows[row]["ITEM_NAME"].ToString() + "'),";
                SQL += ds_prob_items2.Tables[0].Rows[row]["qty"].ToString() + ",";
                SQL += ds_prob_items2.Tables[0].Rows[row]["price"].ToString() + ")";
                oracleacess.ExecuteNonQuery(SQL);
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "upd_prob_items2");
            return false;
        }
    }
    #endregion

    #region "Test & LogError"
    //======================================= Test ========================================

    ////[WebMethod]
    //public int LIVE(int codeRep)
    //{
    //    SQL = "SELECT MESS_CODE FROM PDA_MESS_REPS WHERE REP_CODE =" + codeRep;
    //    Ds.Tables.Clear();
    //    dt = oracleacess.ExecuteDataTable(SQL);
    //    Ds.Tables.Add(dt);
    //    LogError("LIVE", dt.Rows[0][0].ToString());
    //    if(dt.Rows.Count==0)
    //        return 1;
    //    return Convert.ToInt32(dt.Rows[0][0]);
    //}
    //[WebMethod]
    public bool LIVE()
    {
        LogError("Live", "LIve");
        return true;

    }
    //[WebMethod]
    public string read_message(int code)
    {
        LogError("read message", "Can not read message in read_message");
        try
        {
            ///SELECT MESSAGE_BODY FROM PDA_MESSAGES WHERE CODEPDA_MESS_REPS
            SQL = "SELECT MESSAGE_BODY FROM PDA_MESSAGES WHERE CODE=6";
            dt.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            //LogError(SQL, "Get_Reps_by_IMEI_and_IP");
            LogError("read_APPS", dt.Rows[0][0].ToString());
            return dt.Rows[0][0].ToString();
        }
        catch (Exception ex)
        {
            LogError("read message", "Can not read message in read_message");
            return null;
        }
    }
    //   select pda_messages.message_body, pda_messages.send_date From pda_messages,pda_mess_reps
    //where pda_mess_reps.mess_code = pda_messages.code and
    //pda_mess_reps.rep_code = 213 and pda_mess_reps.falg_read=0
    //[WebMethod]
    public DataSet Message(int rep_code)
    {
        try
        {
            Ds.Clear();
            dt.Clear();
            SQL = "select pda_mess_reps.mess_code,pda_messages.message_body, pda_messages.send_date From pda_messages,pda_mess_reps ";
            SQL += "where pda_mess_reps.mess_code = pda_messages.code and ";
            SQL += "pda_mess_reps.rep_code = " + rep_code + " and pda_mess_reps.falg_read = 0";
            SQL += "order by pda_messages.send_date desc";
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
            return Ds;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "MessageError");
            return Ds;
        }
    }
    //[WebMethod]
    public void upd_messages(DataTable dt, int rep_code)
    {
        try
        {
            for (int r = 0; r < dt.Rows.Count; r++)
            {
                SQL = "update pda_mess_reps set READ_DATE= to_date('" + (System.DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"))
                                                + "','MM/DD/YYYY HH24:MI:SS') , FALG_READ = 1  ";
                SQL += "where mess_code = " + dt.Rows[r][0] + " and rep_code=" + rep_code;
                oracleacess.ExecuteNonQuery(SQL);
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Message_Reply Error");
        }
    }
    //[WebMethod]
    public object GetCurrentTime()
    {
        try
        {
            return DateTime.UtcNow;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "GetCurrentTime");
            return null;
        }
    }

    //[WebMethod]
    public DateTime GetTimeFromServer()
    {
        try
        {
            return DateTime.Now;
            //return DateTime.Now.Date.ToString("MM/dd/yyyy");
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "GetTimeFromServer");
            return DateTime.Now;
        }
    }

    //[WebMethod]
    public TimeZone GetTimeZoneFromServer()
    {
        try
        {
            //TimeZoneInfo
            return TimeZone.CurrentTimeZone;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "GetTimeZoneFromServer");
            return null;
        }
    }
    //[WebMethod]
    public object testTime()
    {
        SQL = "select rec_date from eg_viewing where code=3513086";
        object obj = oracleacess.ExecuteScalar(SQL);
        return obj;
    }
    //[WebMethod]
    public string stri()
    {
        return "Helooooooooo";
    }

    //[WebMethod]
    public string TestTime()
    {
        return DateTime.Now.Date.ToString("MM/dd/yyyy HH:mm:ss");
        //DateTime mydate = Convert.ToDateTime("12/01/2011 00:00:00 ص");
        //SQL = "Select * From EG_VIEWING Where PLN_DATE=to_date('" + (Convert.ToDateTime("12/01/2011 09:15:30 PM").ToString("MM/dd/yyyy HH:mm:ss")) 
        //    + "', 'MM/DD/YYYY HH24:MI:SS')";
        //Ds.Tables.Clear();
        //dt = oracleacess.ExecuteDataTable(SQL);
        //Ds.Tables.Add(dt);
        //return Ds;
    }

    //======================================= LogError ====================================
    public void LogError(string msg, string functionName)
    {
        StreamWriter sr = new StreamWriter(@"D:\SRV_OracleAccess\LogError.txt", true);
        string txt = "Time: " + DateTime.Now.ToString() + "\r\n";
        txt += "Function Name: " + functionName + "\r\n";
        txt += "Error Message: " + msg + "\r\n";
        txt += "================================= *** =================================\r\n";

        sr.WriteLine(txt);
        sr.Close();
    }

    #endregion

    #region "Old Code"
    ////[WebMethod]
    //public void UPdateEG_CALL_TRANS(DataSet DTEG_CALL_TRANS)
    //{
    //    //int CODE = 0;
    //    string USER_NAME = "";
    //    for (int row2 = 0; row2 < DTEG_CALL_TRANS.Tables[0].Rows.Count; row2++)
    //    {
    //        object code = 0;
    //        SQL = " SELECT MAX(CODE)+1 FROM EG_CALL_TRANS ";
    //        code = oracleacess.ExecuteScalar(SQL);

    //        if (code == DBNull.Value)
    //        {
    //            code = 1;
    //        }

    //        SQL = "SELECT NAME FROM eg_reps er WHERE er.code=" + Convert.ToInt32(DTEG_CALL_TRANS.Tables[0].Rows[row2]["USER_NAME"].ToString());
    //        USER_NAME = oracleacess.ExecuteScalar(SQL).ToString();


    //        SQL = null;
    //        SQL = "INSERT INTO EG_CALL_TRANS (CODE, CUST_CODE, STATUS, CALLER_TEL, PROBLEM, USER_NAME, REC_DATE, CALL_FROM, ORDER_NO, CALLER_TYPE)";
    //        SQL += "  VALUES(" + Convert.ToInt32(code.ToString()) + ","
    //            + int.Parse(DTEG_CALL_TRANS.Tables[0].Rows[row2]["CUST_CODE"].ToString()) + " , MY_Reverse('"
    //            + DTEG_CALL_TRANS.Tables[0].Rows[row2]["STATUS"].ToString() + "') ,'"
    //            + DTEG_CALL_TRANS.Tables[0].Rows[row2]["CALLER_TEL"].ToString() + "' ,MY_Reverse('"
    //            + DTEG_CALL_TRANS.Tables[0].Rows[row2]["PROBLEM"].ToString() + "'),'"
    //            + USER_NAME + "'"
    //            + ",to_date('" + Convert.ToDateTime(DTEG_CALL_TRANS.Tables[0].Rows[row2]["REC_DATE"])
    //            + "','MM/dd/yyyy') ,MY_Reverse('"
    //            + DTEG_CALL_TRANS.Tables[0].Rows[row2]["CALL_FROM"].ToString() + "' ,"
    //            + int.Parse(DTEG_CALL_TRANS.Tables[0].Rows[row2]["ORDER_NO"].ToString()) + ","
    //            + int.Parse(DTEG_CALL_TRANS.Tables[0].Rows[row2]["CALLER_TYPE"].ToString())
    //            + ")";
    //        oracleacess.ExecuteNonQuery(SQL);
    //        //SQL = null;
    //    }

    ////[WebMethod]
    //public DataSet GetOverLoadViewing(int Rep_COde, DateTime dtm)
    //{
    //    //Select overload Viewing
    //    SQL  = " SELECT EG_VIEWING.CODE, EG_VIEWING.CUST_CODE, EG_VIEWING.ADDRESS, EG_VIEWING.PLN_DATE, EG_VIEWING.OWNER_NAME, EG_VIEWING.TEL, ";
    //    SQL += " EG_VIEWING.MOB, EG_VIEWING.FEEDBACK, EG_CUSTOMERS.NAME, EG_CUSTOMERS.TEL AS CUSTOMERSTel, EG_CUSTOMERS.MOBILE, EG_CUSTOMERS.ID_NO, EG_VIEWING.TITLE, EG_VIEWING.GOV_CODE, EG_VIEWING.CITY_CODE, EG_VIEWING.DIST_CODE, jok_count(CUST_CODE) as AddCard, EG_VIEWING.NOTES, EG_VIEWING.GROUP_mang_code ";
    //    SQL += " FROM EG_VIEWING INNER JOIN ";
    //    SQL += " EG_CUSTOMERS ON EG_VIEWING.CUST_CODE = EG_CUSTOMERS.CODE Where EG_VIEWING.PLN_DATE=Trunc(sysdate) AND EG_VIEWING.REP_CODE=" + Rep_COde + " AND EG_VIEWING.PDA_Status=0";
    //    Ds.Clear();
    //    dt = oracleacess.ExecuteDataTable(SQL);
    //    Ds.Tables.Add(dt);
    //    SQL = " Update EG_VIEWING SET PDA_Status=1 Where PLN_DATE=Trunc(sysdate) AND REP_CODE=" + Rep_COde + "";
    //    oracleacess.ExecuteNonQuery(SQL);
    //    return Ds;
    //}

    ////******** UPDATE VIEWING
    ////[WebMethod]
    //public void UPdateViewing111(DateTime dtm)
    //{
    //    for (int row = 0; row < 1; row++)
    //    {
    //        SQL = "  update EG_VIEWING set FROM_TIME=to_date('" + dtm + " ', 'dd/MM/yyyy HH:MI:SS') ";
    //        //SQL += " ,TO_TIME=to_date('" + DTViewing.Tables[0].Rows[row]["TO_TIME"] + "', 'DD-MM-YYYY HH:MI:SS') ";
    //        //SQL += " ,PDA_Statuse=" + Convert.ToInt32(DTViewing.Tables[0].Rows[row]["PDA_Statuse"].ToString()) + "";
    //        //SQL += " ,Rep_Reject_Reason='" + DTViewing.Tables[0].Rows[row]["Rep_Reject_Reason"].ToString() + "'";
    //        //SQL += "  where CODE=" + DTViewing.Tables[0].Rows[row]["CODE"] + "";
    //        oracleacess.ExecuteNonQuery(SQL);
    //    }
    //}

    ////[WebMethod]
    //public string openconnect()
    //{   try
    //    {
    //    //return the value    
    //        OracleConnection  cnn = new System.Data.OracleClient.OracleConnection ("User ID=APPS;password=APPS;Data Source=EGIC;Persist Security Info=true; Unicode=True");
    //        cnn.Open();
    //        return "open";
    //    }
    //    catch (Exception ex)
    //    {
    //        return "Not Open " + "Exception " + ex.ToString();
    //    }
    //}
    #endregion
    //[WebMethod]
    public DataSet get_all_customers()
    {
        DataSet new_ds = new DataSet();
        SQL = "select * from eg_reps";
        Ds.Tables.Add(oracleacess.ExecuteDataTable(SQL));
        return Ds;
    }
    #region "Rep Codes"

    //[WebMethod]
    public DataSet GetBacicCode_Reps(string Criteria)
    {
        //EG_REPS
        //SQL = "select CODE,UserName,Password,ACTIVE from EG_REPS where 1=1";
        SQL = " Select CODE, UserName, Name, Password, ACTIVE, REP_GOV(CODE) as GovCode, MOBILE FROM EG_REPS Where LOWER(ACTIVE)='y'";
        if (Criteria != "")
        {
            SQL += " AND CODE=" + Criteria + "";
        }
        SQL += " order by Name";
        Ds.Tables.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }



    //[WebMethod]
    public DataSet Get_Reps_by_IMEI_and_IP(string IMEI)
    {
        DataSet new_ds = new DataSet();
        //DataTable dtt = new DataTable();
        //EG_REPS
        //SQL = " Select CODE, UserName, Name, Password, ACTIVE, REP_GOV(CODE) as GovCode, MOBILE FROM EG_REPS Where LOWER(ACTIVE)='y'";
        try
        {
            SQL = " Select CODE, UserName,Name,  Password, ACTIVE, REP_GOV(CODE) as GovCode, MOBILE,rep_permission FROM EG_REPS ";
            SQL += " Where LOWER(ACTIVE)='y'and PDA_IMEI='" + IMEI + "'";
            //SQL = "Select er.CODE, er.UserName, er.Password, er.Name,er.MOBILE,rp.FRMSETTING,rp.FRMVIEW,rp.FRMTECHINFO,rp.FRMREPPROD,rp.FRMTRANS,rp.FRMCOBONS,rp.UPDATES ";
            //SQL += "FROM EG_REPS er,pda_rep_priv rp ";
            //SQL += "where er.CODE = rp.REP_CODE and LOWER(ACTIVE)='y' and PDA_IMEI='" + IMEI + "'";
            new_ds.Tables.Add(oracleacess.ExecuteDataTable(SQL));
            if (new_ds.Tables[0].Rows.Count > 0)
                return new_ds;
            else
                return null;
            //---------------------------------------------------------------------------------------------------------

            // and PDA_IP='" + IP + "'";
            //SQL += " order by Name";
            //Ds.Tables.Clear();
            //dt.Clear();

            //Ds.Tables.Add(dt);
            //dt.Clear();
            //LogError(new_ds.Tables[0].Rows.Count.ToString(),"check ");
            //LogError(SQL, "Get_Reps_by_IMEI_and_IP");
            //return Ds;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Reps_by_IMEI_and_IP");
            return null;
        }
    }
    //[WebMethod]
    public DataSet Get_Reps_by_IMEI_New(string IMEI)
    {
        DataSet new_ds = new DataSet();
        //DataTable dtt = new DataTable();
        //EG_REPS
        //SQL = " Select CODE, UserName, Name, Password, ACTIVE, REP_GOV(CODE) as GovCode, MOBILE FROM EG_REPS Where LOWER(ACTIVE)='y'";
        try
        {
            //SQL = " Select CODE, UserName,Name,  Password, ACTIVE, REP_GOV(CODE) as GovCode, MOBILE,rep_permission FROM EG_REPS ";
            //SQL += " Where LOWER(ACTIVE)='y'and PDA_IMEI='" + IMEI + "'";
            SQL = "Select er.CODE, er.UserName, er.Password, er.Name,er.MOBILE,rp.FRMSETTING,rp.FRMVIEW,rp.FRMTECHINFO,rp.FRMREPPROD,rp.FRMTRANS,rp.FRMCOBONS,rp.UPDATES,To_Char(er.load_from,'HH24'),to_char(er.load_to,'HH24') ";
            SQL += "FROM EG_REPS er,pda_rep_priv rp ";
            SQL += "where er.CODE = rp.REP_CODE and LOWER(er.ACTIVE)='y' and er.PDA_IMEI='" + IMEI + "'";
            new_ds.Tables.Add(oracleacess.ExecuteDataTable(SQL));
            if (new_ds.Tables[0].Rows.Count > 0)
                return new_ds;
            else
                return null;
            //---------------------------------------------------------------------------------------------------------

            // and PDA_IP='" + IP + "'";
            //SQL += " order by Name";
            //Ds.Tables.Clear();
            //dt.Clear();

            //Ds.Tables.Add(dt);
            //dt.Clear();
            //LogError(new_ds.Tables[0].Rows.Count.ToString(),"check ");
            //LogError(SQL, "Get_Reps_by_IMEI_and_IP");
            //return Ds;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Reps_by_IMEI_New");
            return null;
        }
    }

    #endregion
    #region "Get_Product"
    //[WebMethod]
    public DataSet Get_PROB_KIND()
    {
        //EG_REPS
        //SQL = "select CODE,UserName,Password,ACTIVE from EG_REPS where 1=1";
        SQL = " Select distinct PROB_KIND FROM QA_RESP_PROB order by PROB_KIND";
        //if (Criteria != "")
        //{
        //    SQL += " AND CODE=" + Criteria + "";
        //}
        //SQL += " order by PROB_KIND";
        Ds.Tables.Clear();
        dt = oracleacess.ExecuteDataTable(SQL);
        Ds.Tables.Add(dt);
        return Ds;
    }
    #endregion
    #region "Update_Application"
    //[WebMethod]
    public string GetVersion(string PDA_Version)
    {
        //LogError(PDA_Version, "DB1");
        string Version = "-1";
        System.Reflection.Assembly FilePath = System.Reflection.Assembly.LoadFrom(@"D:\SRV_OracleAccess\EGIC_UpdateVersion\EGIC.exe");

        string fullVersionSpec = FilePath.FullName.Split(',')[1];
        string ServerVersion = fullVersionSpec.Substring(fullVersionSpec.IndexOf('=') + 1);
        if (PDA_Version != ServerVersion)
        {
            Version = ServerVersion;
        }

        return Version;
    }

    //[WebMethod]
    public string Check_Version(string PDA_Version, string VERTYPE, int RepCode)
    {
        //LogError(PDA_Version, "DB1");
        object ORA_VER_NO = null;
        string version = "1";
        try
        {
            //    //SQL = " Select VER_NO From PDA_VER_REPS Where CODE IN ( Select max(CODE) From PDA_VER_REPS Where VER_TYPE='" + VERTYPE + "' AND AVL='Y' AND REP_CODE=" + RepCode + ")";
            SQL = " Select PDA_VERSIONS.VER_NO From  PDA_VERSIONS Where PDA_VERSIONS.VER_TYPE='" + VERTYPE + "' and PDA_VERSIONS.AVL='Y' and PDA_VERSIONS.VER_NO "
                + " IN( Select PDA_VER_REPS.VER_NO From PDA_VER_REPS Where PDA_VER_REPS.CODE IN "
                + " ( Select max(CODE) From PDA_VER_REPS Where PDA_VER_REPS.VER_TYPE='" + VERTYPE + "'and PDA_VER_REPS.REP_CODE=" + RepCode + " and PDA_VER_REPS.AVL='Y'))";
            ORA_VER_NO = oracleacess.ExecuteScalar(SQL);

            //    //System.Reflection.Assembly FilePath = System.Reflection.Assembly.LoadFrom(@"D:\SRV_OracleAccess\EGIC_UpdateVersion\EGIC.exe");
            //    //string fullVersionSpec = FilePath.FullName.Split(',')[1];
            //    //string ServerVersion = fullVersionSpec.Substring(fullVersionSpec.IndexOf('=') + 1);

            //    //LogError(SQL, "Check_Version");
            //    //LogError(PDA_Version, "Check_Version - PDA_Version");

            //    //if (ServerVersion == ORA_VER_NO)
            //    //{
            //    //if (PDA_Version != ServerVersion)

            //    //string namee = "";
            //    //SQL = " SELECT NAME  eg_reps er WHERE CODE=" + RepCode+ "";
            //    //namee = oracleacess.ExecuteScalar(SQL).ToString();


            if (PDA_Version != ORA_VER_NO.ToString())
            {
                //version = ServerVersion;
                version = ORA_VER_NO.ToString();
                //Insert_INTO_Update_Log(RepCode, ORA_VER_NO.ToString(), VERTYPE, DateTime.Now.Date.ToString("MM/dd/yyyy"), "database");
                Insert_INTO_Update_Log(RepCode, ORA_VER_NO.ToString(), VERTYPE, "S", "Check");
            }
            else
            {
                version = "-1";
                //Insert_INTO_Update_Log(RepCode, ORA_VER_NO.ToString(), VERTYPE, "F", "Check");
            }
            //    //}
            //    //else
            //    //{
            //    //    version = "-1";
            //    //    Insert_INTO_Update_Log(RepCode, PDA_Version, VERTYPE, "F", "Check");
            //    //}

            //    LogError(SQL, "return Check_Version");

            return version;
        }
        catch (Exception ex)
        {
            LogError(SQL, "Check_Version");
            LogError(ex.Message, "Check_Version");
            return version;
        }
    }

    //[WebMethod]
    public bool Insert_INTO_Update_Log(int Rep_CODE, string VER_NO, string VER_TYPE, string STATUS, string LOG_TYPE)
    {
        try
        {
            SQL = " Insert INTO PDA_VER_LOG(Rep_CODE, VER_NO, VER_TYPE,  STATUS, LOG_TYPE )Values("
                 + Rep_CODE + ",'" + VER_NO + "','" + VER_TYPE + "', '" + STATUS + "','" + LOG_TYPE + "')";
            LogError(SQL, "Insert_INTO_Update_Log");
            oracleacess.ExecuteNonQuery(SQL);
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Insert_INTO_Update_Log");
            return false;
        }
    }
    //[WebMethod]
    public bool update_priv(int rep_code, string ver_type)
    {
        DataTable dt = new DataTable();
        SQL = "select distinct avl from pda_ver_reps where rep_code=" + rep_code + " and ver_type='" + ver_type + "'";
        dt = oracleacess.ExecuteDataTable(SQL);
        if (dt.Rows[0][0].ToString() == "Y")
        {
            return true;
        }
        else
            return false;
    }
    #endregion "Update_Application"
    #region Traders_cards
    //[WebMethod]
    public DataSet GetCardData(double cardno)
    {
        Ds.Clear();
        dt.Clear();
        //DataTable dt = new DataTable();
        try
        {
            SQL = "select inv_type ,SLC_DISTRIBUTORS.code as dist_code,SLC_DISTRIBUTORS.shop_name,issue_month  from slc_card_issue,slc_DISTRIBUTORS where slc_card_issue.dist_code = SLC_DISTRIBUTORS.code and " + cardno + " between from_no and to_no";//1310000029";
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "GetCardData");
        }
        return Ds;
    }
    //[WebMethod]
    public DataSet Get_Traders()
    {
        SQL = "select * from slc_traders";
        Ds.Tables.Add(dt = oracleacess.ExecuteDataTable(SQL));
        return Ds;
    }
    //[WebMethod]
    public bool Insert_Card_Trader(string card_no, int trader_code, int user_name)
    {
        try
        {
            SQL = "select * from slc_card_rec where card_no =" + card_no;
            DataTable dc = oracleacess.ExecuteDataTable(SQL);
            if (dc.Rows.Count == 0)
            {
                SQL = "insert into slc_card_rec ( card_no,trader_code,user_name) values (";
                SQL += card_no + "," + trader_code + ",'" + user_name + "')";
                oracleacess.ExecuteNonQuery(SQL);
                return true;
            }
            else
                return false;

        }
        catch (Exception ex)
        { LogError(ex.ToString(), "Insert_Card_Trader"); return false; }//.ToString("MM/dd/yyyy HH:mm:ss"))+ "','MM/DD/YYYY HH24:MI:SS'
    }
    #endregion traders_cards
    //=========================== Cobons ==========================================
    #region Cobons
    //[WebMethod]
    public bool check_rep(int rep_code)
    {
        bool flag = false;
        try
        {
            dt.Clear();
            SQL = "select distinct rep_code from eg_rep_cities where gov_code in (12,13,21) order by rep_code";
            dt = oracleacess.ExecuteDataTable(SQL);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (Convert.ToInt16(dt.Rows[i][0]) == rep_code)
                    flag = true;
            }
            return flag;
        }
        catch (Exception ex)
        {
            LogError(ex.ToString(), "check_rep");
            return flag;
        }
    }
    //[WebMethod]
    public DataSet get_shops(int rep_code)
    {
        Ds.Clear(); dt.Clear();
        try
        {
            SQL = " SELECT CODE SHOP_CODE,NAME SHOP_NAME ,gov_code,city_code,dist_code FROM EG_SHOPS ";
            SQL += " WHERE DIST_CODE IN ( SELECT DIST_CODE FROM EG_REP_CITIES WHERE REP_CODE = " + rep_code + " ) and city_code is not null and dist_code is not null order by NAME";
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt.Copy());
            return Ds;
        }
        catch (Exception ex)
        {
            LogError(ex.ToString(), "get_shops");
            return null;
        }
    }
    //[WebMethod]
    public DataSet get_cust(int rep_code)
    {
        DataTable temp = new DataTable();
        Ds.Clear(); dt.Clear();
        try
        {
            //SQL = "SELECT CODE CUST_CODE,NAME CUST_NAME ,gov_code,city_code,dist_code FROM EG_CUSTOMERS ";
            //SQL += "WHERE SHOP_CODE IN ( SELECT CODE FROM EG_SHOPS ";
            //SQL += "WHERE DIST_CODE IN(SELECT DIST_CODE FROM EG_REP_CITIES WHERE REP_CODE = " + rep_code + ")) and city_code is not null and dist_code is not null order by name";
            SQL = "select code CUST_CODE, name CUST_NAME, gov_code,city_code , dist_CODE from eg_customers where dist_code in ( select dist_code from eg_rep_cities where rep_code = " + rep_code + " ) order by name";
            dt = oracleacess.ExecuteDataTable(SQL);
            SQL = "select code CUST_CODE, name CUST_NAME, gov_code,city_code , dist_CODE from eg_customers where code = 8397";
            temp = oracleacess.ExecuteDataTable(SQL);
            DataRow dr = dt.NewRow();
            dr[0] = temp.Rows[0][0];
            dr[1] = temp.Rows[0][1];
            dr[2] = temp.Rows[0][2];
            dr[3] = temp.Rows[0][3];
            dr[4] = temp.Rows[0][4];
            dt.Rows.Add(dr);
            Ds.Tables.Add(dt.Copy());
            return Ds;
        }
        catch (Exception ex)
        {
            LogError(ex.ToString(), "get_cust");
            return null;
        }
    }
    //[WebMethod]
    public bool upd_cobon_survey(DataSet ds, object rep_name, int repCode)
    {
        string code = null;
        try
        {

            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                code = repCode.ToString() + ds.Tables[0].Rows[i][4].ToString();
                SQL = "insert into cobon_survey values ( "
                    + ds.Tables[0].Rows[i][0] + "," + ds.Tables[0].Rows[i][1] + "," + ds.Tables[0].Rows[i][2]
                    + ",MY_Reverse('" + ds.Tables[0].Rows[i][3] + "'),'" + rep_name + "',sysdate," + code + ")";
                oracleacess.ExecuteNonQuery(SQL);
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.ToString(), "upd_cobon_survey"); return false;
        }
    }
    #endregion get data for cobons
    //========================== Seminars =========================================
    #region Seminars
    //[WebMethod]
    public DataSet getSeminars(int rep_code)
    {
        DataTable seminars = new DataTable();
        DataTable semClients = new DataTable();
        try
        {
            Ds.Clear();
            //dt.Clear();
            SQL = "select code,sem_no,name,sem_date,address,sem_type,reason,sem_time from gs_sem_main where rep_code = " + rep_code + "and load_status = 0";
            seminars = oracleacess.ExecuteDataTable(SQL);
            if (seminars.Rows.Count > 0)
            {
                Ds.Tables.Add(seminars);
                SQL = "select cust_name,mobile,notes,main_code from gs_sem_det where main_code in (select code from gs_sem_main where rep_code = " + rep_code + ")";
                semClients = oracleacess.ExecuteDataTable(SQL);
                if (semClients.Rows.Count > 0)
                {
                    Ds.Tables.Add(semClients);
                }
            }
            return Ds;
        }
        catch (Exception ex)
        {
            LogError(ex.ToString(), "getSeminars");
            return null;
        }
    }

    //[WebMethod]
    public bool updateSeminarStatus(DataSet semTable, int load_status)
    {
        try
        {
            for (int i = 0; i < semTable.Tables[0].Rows.Count; i++)
            {
                SQL = "update gs_sem_main set load_status = " + load_status
                    + " where code = " + semTable.Tables[0].Rows[i][0];
                oracleacess.ExecuteNonQuery(SQL);
            }
            return true;
        }
        catch (Exception ex)
        {
            LogError(ex.ToString(), "updateSeminarStatus");
            return false;
        }
    }
    //[WebMethod]
    public bool setSeminarsClients(DataSet semClientsTable)
    {

        //main_code,cust_name,mobile,notes // coming fields
        try
        {
            SQL = "delete from gs_sem_det where main_code = " + semClientsTable.Tables[0].Rows[0][0];
            oracleacess.ExecuteNonQuery(SQL);
            object code = 0;
            for (int i = 0; i < semClientsTable.Tables[0].Rows.Count; i++)
            {
                SQL = " SELECT MAX(CODE)+1 FROM gs_sem_det ";
                code = oracleacess.ExecuteScalar(SQL);
                int is_arranged = Convert.ToInt16(semClientsTable.Tables[0].Rows[i][4]);
                if (is_arranged == 0)
                {

                    SQL = "insert into gs_sem_det (code , main_code , cust_name , mobile , notes ) ";
                    SQL += "values (" + code.ToString() + "," + semClientsTable.Tables[0].Rows[i][0] + ", ";
                    SQL += "MY_Reverse ('" + semClientsTable.Tables[0].Rows[i][1] + "'), ";
                    SQL += "'" + semClientsTable.Tables[0].Rows[i][2] + "', ";
                    SQL += "MY_Reverse('" + semClientsTable.Tables[0].Rows[i][3] + "'))";
                }
                else
                {
                    SQL = "insert into gs_sem_det (code , main_code , cust_name , mobile , notes ) ";
                    SQL += "values (" + code.ToString() + "," + semClientsTable.Tables[0].Rows[i][0] + ", ";
                    SQL += "'" + semClientsTable.Tables[0].Rows[i][1] + "', ";
                    SQL += "'" + semClientsTable.Tables[0].Rows[i][2] + "', ";
                    SQL += "'" + semClientsTable.Tables[0].Rows[i][3] + "')";

                }
                oracleacess.ExecuteNonQuery(SQL);
            }
            if (updateSeminarStatus(semClientsTable, 2))
                return true;
            else
                return false;

        }
        catch (Exception ex)
        {
            LogError(ex.ToString(), "setSeminarsClients");
            return false;
        }
    }
    #endregion Seminars
}
