﻿using System;
using System.Data;
using System.IO;
using System.Web.Services;
using System.Xml.Serialization;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class Service : System.Web.Services.WebService
{
    OracleAcess oracleacess = new OracleAcess("Bill_App_Conn");
    string SQL = "";
    DataSet Ds = new DataSet();
    DataTable dt = new DataTable();

    public Service()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //*********************************************************************** ( Users $ Password  ) ***************************
    #region " User Settings "
    [WebMethod]
    public bool Check_Loged_User (string User_Name , string Password)
    {
        bool ret_val = false ;

        try
        {
            //string INC_PASS = Password.Insert(0, "W") ;
            //string New_PASS = INC_PASS.Insert(4, "YM");
            SQL = "SELECT PASSWORD FROM BL_USERS WHERE USER_NAME = '"+ User_Name.ToUpper().ToString() +"' AND STATUS = '1' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString().ToUpper().Trim() == Password.ToUpper().Trim())
            {
                ret_val = true;
            }
            else
            {
                ret_val = false;
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Loged_User >> to check logged user and password");
            ret_val = false;
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_User_Name(string User_Name)
    {
        string ret_val = "";

        try
        {
            SQL = " select full_name from bl_users where user_name = '" + User_Name + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_User_Name >> To get User Name");
        }

        return ret_val;
    }

    [WebMethod]
    public int Check_Page_Security(string page_name , string user , string Prev )
    {
        int val = 0;
        try
        {

            SQL = "  select count(*) from bl_users_pages a , bl_pages p , bl_users u , bl_permissions v where a.user_id = u.id and       a.page_id = p.page_code  AND      v.perm_id = a.perm_id and  u.user_name = '"+user+"' AND Page_name = '"+page_name+"' AND v.perm_name = '"+Prev+"'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            val = int.Parse(Pass.ToString());

        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Page_Security >> ");
            val = 0;
        }

        return val;
    }

    #endregion
    //*********************************************************************** ( Internal Func   ) *****************************
    #region " Internal Functions "

    public void LogError(string msg, string functionName)
    {
        StreamWriter sr = new StreamWriter(@"C:\Inetpub\wwwroot\Vodafone_Bill_WS\LogError.txt", true);
        string txt = "Time: " + DateTime.Now.ToString() + "\r\n";
        txt += "Function Name: " + functionName + "\r\n";
        txt += "Error Message: " + msg + "\r\n";
        txt += "================================= *** =================================\r\n";

        sr.WriteLine(txt);
        sr.Close();
    }

    #endregion
    //*********************************************************************** ( Customers       ) *****************************
    #region " Vodafone Lines Master Data   "

    [WebMethod]
    public DataSet Bind_EMP_DDL()
    {
        try
        {
            SQL = " select emp_no , emp_no ||' - '|| emp_name as emp_name from pay_egic.BIL_EMP ORDER BY emp_no  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_EMP_DDL >> Fill emp to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_LINES_GRID()
    {
        try
        {
            //SQL = " select  bl_lines.line_no , emp.emp_no ,emp.emp_name,SECTION_NAME , bl_lines.app_limit , bl_lines.inter_calls , bl_lines.aftr_sal_tax , bl_lines.comp_shar , bl_lines.emp_shar , bl_lines.notes ,bl_lines.created_user , bl_lines.created_date , bl_lines.L_STATUS , bl_lines.I_STATUS , bl_lines.REF_DATA  from pay_egic.emp,pay_egic.SECTION,BL_LINES where emp.SECTION_NO=SECTION.SECTION_NO and emp.emp_no = bl_lines.emp_no and emp.LEAV_NO is null order by 1 ";
            SQL = " select  bl_lines.line_no , BIL_EMP.emp_no ,BIL_EMP.emp_name, BIL_EMP.SECTION_NAME , bl_lines.app_limit , bl_lines.inter_calls ,         bl_lines.aftr_sal_tax , bl_lines.comp_shar , bl_lines.emp_shar , bl_lines.notes ,bl_lines.created_user ,        bl_lines.created_date , bl_lines.L_STATUS , bl_lines.I_STATUS , bl_lines.REF_DATA , bl_lines.rd_date , bl_lines.dd_date , bl_lines.line_status ,  bl_lines.rate_plan , bl_lines.line_type , bl_lines.tax_flag ,  bl_lines.tax_val , bl_lines.sal_tax_per ,  bl_lines.ID , tbl_tax_per , new_sales_tax_per  from pay_egic.BIL_EMP , BL_LINES  where  BIL_EMP.emp_no = bl_lines.emp_no  order by 1 ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_LINES_GRID");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_LINES_GRID_By_Line(string line_no )
    {
        try
        {
           // SQL = " select  bl_lines.line_no , emp.emp_no ,emp.emp_name,SECTION_NAME , bl_lines.app_limit , bl_lines.inter_calls , bl_lines.aftr_sal_tax , bl_lines.comp_shar , bl_lines.emp_shar , bl_lines.notes ,bl_lines.created_user , bl_lines.created_date , bl_lines.L_STATUS , bl_lines.I_STATUS , bl_lines.REF_DATA  from pay_egic.emp,pay_egic.SECTION,BL_LINES where emp.SECTION_NO=SECTION.SECTION_NO and emp.emp_no = bl_lines.emp_no and emp.LEAV_NO is null and bl_lines.line_no = '"+line_no+"' order by 1 ";
            SQL = " select  bl_lines.line_no , BIL_EMP.emp_no ,BIL_EMP.emp_name, BIL_EMP.SECTION_NAME , bl_lines.app_limit , bl_lines.inter_calls ,         bl_lines.aftr_sal_tax , bl_lines.comp_shar , bl_lines.emp_shar , bl_lines.notes ,bl_lines.created_user ,        bl_lines.created_date , bl_lines.L_STATUS , bl_lines.I_STATUS , bl_lines.REF_DATA , bl_lines.rd_date , bl_lines.dd_date , bl_lines.line_status ,  bl_lines.rate_plan , bl_lines.line_type  , bl_lines.tax_flag ,  bl_lines.tax_val ,  bl_lines.sal_tax_per  ,  bl_lines.ID , tbl_tax_per , new_sales_tax_per    from pay_egic.BIL_EMP , BL_LINES  where  BIL_EMP.emp_no = bl_lines.emp_no and bl_lines.line_no = '" + line_no + "'  order by 1 ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_LINES_GRID_By_Line");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_LINES_GRID_By_Emp(string Emp_no)
    {
        try
        {
            //SQL = " select  bl_lines.line_no , emp.emp_no ,emp.emp_name,SECTION_NAME , bl_lines.app_limit , bl_lines.inter_calls , bl_lines.aftr_sal_tax , bl_lines.comp_shar , bl_lines.emp_shar , bl_lines.notes ,bl_lines.created_user , bl_lines.created_date , bl_lines.L_STATUS , bl_lines.I_STATUS , bl_lines.REF_DATA  from pay_egic.emp,pay_egic.SECTION,BL_LINES where emp.SECTION_NO=SECTION.SECTION_NO and emp.emp_no = bl_lines.emp_no and emp.LEAV_NO is null and emp.emp_no = '" + Emp_no + "' order by 1 ";
            SQL = " select  bl_lines.line_no , BIL_EMP.emp_no ,BIL_EMP.emp_name, BIL_EMP.SECTION_NAME , bl_lines.app_limit , bl_lines.inter_calls ,         bl_lines.aftr_sal_tax , bl_lines.comp_shar , bl_lines.emp_shar , bl_lines.notes ,bl_lines.created_user ,        bl_lines.created_date , bl_lines.L_STATUS , bl_lines.I_STATUS , bl_lines.REF_DATA , bl_lines.rd_date , bl_lines.dd_date , bl_lines.line_status  ,  bl_lines.rate_plan , bl_lines.line_type    , bl_lines.tax_flag ,  bl_lines.tax_val , bl_lines.sal_tax_per ,  bl_lines.ID , tbl_tax_per , new_sales_tax_per   from pay_egic.BIL_EMP , BL_LINES  where  BIL_EMP.emp_no = bl_lines.emp_no and BIL_EMP.emp_no = '" + Emp_no + "'  order by 1 ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_LINES_GRID_By_Emp");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_LINES_GRID_By_NAME(string Emp_name)
    {
        try
        {
            //SQL = " select  bl_lines.line_no , emp.emp_no ,emp.emp_name,SECTION_NAME , bl_lines.app_limit , bl_lines.inter_calls , bl_lines.aftr_sal_tax , bl_lines.comp_shar , bl_lines.emp_shar , bl_lines.notes ,bl_lines.created_user , bl_lines.created_date , bl_lines.L_STATUS , bl_lines.I_STATUS , bl_lines.REF_DATA  from pay_egic.emp,pay_egic.SECTION,BL_LINES where emp.SECTION_NO=SECTION.SECTION_NO and emp.emp_no = bl_lines.emp_no and emp.LEAV_NO is null and emp.emp_no = '" + Emp_no + "' order by 1 ";
            SQL = " select  bl_lines.line_no , BIL_EMP.emp_no ,BIL_EMP.emp_name, BIL_EMP.SECTION_NAME , bl_lines.app_limit , bl_lines.inter_calls ,         bl_lines.aftr_sal_tax , bl_lines.comp_shar , bl_lines.emp_shar , bl_lines.notes ,bl_lines.created_user ,        bl_lines.created_date , bl_lines.L_STATUS , bl_lines.I_STATUS , bl_lines.REF_DATA  , bl_lines.rd_date , bl_lines.dd_date , bl_lines.line_status ,  bl_lines.rate_plan , bl_lines.line_type , bl_lines.tax_flag ,  bl_lines.tax_val , bl_lines.sal_tax_per ,  bl_lines.ID ,  tbl_tax_per , new_sales_tax_per  from pay_egic.BIL_EMP , BL_LINES   where  BIL_EMP.emp_no = bl_lines.emp_no and BIL_EMP.emp_name LIKE '" + Emp_name + "%'  order by 1 ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_LINES_GRID_By_NAME");
        }
        return Ds;
    }

    [WebMethod]
    public int Save_Lines_Data(
        string line_no, string emp_no, string app_limit, string inter_calls, 
        string aftr_sal_tax, string comp_shar, string emp_shar, string created_user, 
        string notes, string L_status, string I_status, string Ref_Data, string rc_date, 
        string dd_date, string line_status , string rate_plan , string line_type , 
        string taxflag , string taxval , string saltax , string tblTax , string addedTax )
    {
        int val = 0;
        try
        {
            string created_date = DateTime.Now.ToString("dd/MM/yy");
            SQL = " INSERT INTO BL_LINES ( LINE_NO , EMP_NO , APP_LIMIT , INTER_CALLS , AFTR_SAL_TAX ,"+
                  " COMP_SHAR , EMP_SHAR , CREATED_USER , CREATED_DATE , NOTES , L_STATUS ,I_STATUS , "+
                  " REF_DATA , rd_date , dd_date , line_status , rate_plan , line_type , Tax_flag , "+
                  " Tax_Val , Sal_TAX_PER, TBL_TAX_PER , new_sales_tax_per ) ";
            SQL += " VALUES ( '" + line_no + "','" + emp_no + "','" + app_limit + "','" + inter_calls + "','" + aftr_sal_tax +
                "','" + comp_shar + "','" + emp_shar + "','" + created_user + "','" + created_date +
                "','" + notes + "','" + L_status + "','" + I_status + "' , '" + Ref_Data + "' , '" + rc_date +
                "' , '" + dd_date + "' , '" + line_status + "','" + rate_plan + "','" + line_type + "' , '" + taxflag + "' , '" + taxval + "' , '" + saltax + "', '" + tblTax + "' , '"+addedTax+"' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Lines_Data >> save Lines Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Update_Lines_Data( string id , string line_no, string emp_no, string app_limit, string inter_calls, string aftr_sal_tax, string comp_shar, string emp_shar, string notes, string L_status, string I_status, string ref_data, string rc_date, string dd_date, string line_status, string rate_plan, string line_type ,  string taxflag , string taxval , string saltax , string tbl_tax , string addedtax)
    {
        int val = 0;
        try
        {

            SQL = " Update BL_LINES Set EMP_NO = '" + emp_no + "' , APP_LIMIT = '" + app_limit + "' , INTER_CALLS = '" + inter_calls + "' , AFTR_SAL_TAX = '" + aftr_sal_tax + "' , COMP_SHAR = '" + comp_shar + "' ,EMP_SHAR = '" + emp_shar + "' , NOTES = '" + notes + "', L_STATUS = '" + L_status + "', I_STATUS = '" + I_status + "' , REF_DATA = '" + ref_data + "', rd_date = '" + rc_date + "' ,  dd_date = '" + dd_date + "' ,  line_status = '" + line_status + "', rate_plan = '" + rate_plan + "', line_type = '" + line_type + "' , Tax_flag = '" + taxflag + "' , Tax_Val = '" + taxval + "' , Sal_Tax_Per = '" + saltax + "' , tbl_tax_per = '"+tbl_tax+"' , new_sales_tax_per = '"+addedtax+"'  WHERE ID = '" + id + "'  and LINE_NO = '" + line_no + "'    ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError( ex.Message , "Update_Lines_Data >> Update lines Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Delet_Lines_Data(string line_no, string emp_no , string id )
    {
        int val = 0;
        try
        {

            SQL = " Delete From BL_LINES  WHERE ID = '"+id+"' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch(Exception ex)
        {
            LogError(ex.Message, "Delet_Lines_Data >> delete lines Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Check_Saved_Line_No(string Line_No , string Emp_no )
    {
        int ret_val = 0 ;

        try
        {
            SQL = "  select Count(*) from bl_lines where line_no = '"+Line_No+"' and emp_no = '"+Emp_no+"' and line_status = '0'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Saved_Line_No >> To check entry of line no ");
        }

        return ret_val;
    }

    [WebMethod]
    public int Check_Line_No(string Line_No)
    {
        int ret_val = 0;

        try
        {
            SQL = "  select Count(*) from bl_lines where line_no = '" + Line_No + "' and line_status = '0'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Line_No >> To check entry of line no ");
        }

        return ret_val;
    }



    #endregion
    //*******************************************************************************************
    #region " Out Sourcing Employees   "

    [WebMethod]
    public DataSet Bind_Dep_DDL()
    {
        try
        {
            SQL = "  select section_no , section_name from pay_egic.section  where section_no in ( select section_no from pay_egic.emp ) order by section_name";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Dep_DDL >> Fill department to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Job_DDL()
    {
        try
        {
            SQL = "  select job_no , job_name from pay_egic.job ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Job_DDL >> Fill job to DDL");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_OSE_GRID()
    {
        try
        {
            SQL = " SELECT E.EMP_NO , E.EMP_NAME , S.SECTION_NAME , J.JOB_NAME FROM pay_egic.OS_EMP E , pay_egic.SECTION S, pay_egic.JOB J WHERE E.JOB_NO = J.JOB_NO AND E.SECTION_NO = S.SECTION_NO ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_OSE_GRID");
        }
        return Ds;
    }

    [WebMethod]
    public int Save_OSE(string emp_no, string emp_name, string section_code , string job_code )
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO  pay_egic.OS_EMP ( EMP_NO , EMP_NAME , SECTION_NO , JOB_NO ) VALUES ( '"+emp_no+"' , '"+emp_name+"' , '"+section_code+"' , '"+job_code+"'  ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_OSE >> save OSE Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Update_OSE(string emp_no, string emp_name, string section_code, string job_code)
    {
        int val = 0;
        try
        {

            SQL = " Update  pay_egic.OS_EMP Set EMP_NAME = '"+emp_name+"' , SECTION_NO = '"+section_code+"' , JOB_NO = '"+job_code+"' WHERE EMP_NO = '"+emp_no+"' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_OSE >> Update OSE Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Delete_OSE(string emp_no )
    {
        int val = 0;
        try
        {

            SQL = " Delete From pay_egic.OS_EMP WHERE EMP_NO = '" + emp_no + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_OSE >> delete OSE Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Check_OSE_In_PYEGIC(string emp_no)
    {
        int ret_val = 0 ;

        try
        {
            SQL = "  select count(*) from PAY_EGIC.Bil_Emp where emp_no = '" + emp_no + "' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_OSE_In_PYEGIC >> ");
        }

        return ret_val;
    }

    [WebMethod]
    public int Check_OSE_Have_Line(string emp_no)
    {
        int ret_val = 0;

        try
        {
            SQL = " select count(*) from bl_lines where emp_no = '"+emp_no+"'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_OSE_Have_Line >> ");
        }

        return ret_val;
    }

    #endregion
    //*********************************************************************** ( Reps Data       ) *****************************
    #region " Vodafone Users Data "

    [WebMethod]
    public string Get_Next_User_ID()
    {
        string ret_val = "";

        try
        {
            SQL = " select max(ID)+1 from BL_USERS   ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = "1";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Next_User_ID  ");
        }

        return ret_val;
    }

    [WebMethod]
    public DataSet Bind_Users_GRID()
    {
        try
        {
            SQL = " select ID , USER_NAME ,Full_NAME , Password , Status FROM BL_USERS ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Users_GRID ");
        }
        return Ds;
    }

    [WebMethod]
    public int Save_Users_Data(string ID, string user_name, string full_name, string password, string status )
    {
        int val = 0;
        try
        {
            SQL = " INSERT INTO BL_USERS ( ID , USER_NAME , FULL_NAME ,PASSWORD, STATUS ) ";
            SQL += " VALUES (  '" + ID + "' ,  '" + user_name + "' ,  '" + full_name + "' ,  '" + password + "' ,  '" + status + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Users_Data >> ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Update_Users_Data(string ID, string user_name, string full_name, string password, string status)
    {
        int val = 0;
        try
        {

            SQL = " Update BL_USERS Set USER_NAME = '" + user_name + "' ,FULL_NAME = '" + full_name + "' , PASSWORD = '" + password + "' , STATUS = '"+status+"' WHERE ID = '" + ID + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Update_Users_Data ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Delete_Users_Data(string ID)
    {
        int val = 0;
        try
        {

            SQL = " Delete From BL_USERS  WHERE ID = '" + ID + "'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_Users_Data  ");
            val = 0;
        }

        return val;
    }

#endregion
    //**********************************************************************
    #region " Vodafone Billing Data "

    [WebMethod]
    public string Get_Next_Bill_ID()
    {
        string ret_val = "";

        try
        {
            SQL = " select MAX(BILL_NO)+1 from BL_BILL_H  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = "1";
            }
            else
            {
                ret_val = Pass.ToString();
            }
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Next_Bill_ID  ");
        }

        return ret_val;
    }

    [WebMethod]
    public DataSet Bind_Billing_GRID()
    {
        try
        {
            SQL = " select BILL_NO , MONTH , YEAR , NOTE , CREATED_USER , CRAETED_DATE FROM BL_BILL_H ORDER BY BILL_NO DESC ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Billing_GRID ");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Bind_Details_Billing_GRID(string bill_no)
    {
        try
        {
            SQL = " SELECT BILL_NO , LINE_NO , RATE_PLAN , MON_CHRGS , NAT_CHRGS , INTR_CHRGS , ROM_CHRGS , OTHR_CHRGS FROM BL_BILL_D WHERE BILL_NO = '"+ bill_no +"' ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Details_Billing_GRID ");
        }
        return Ds;
    }

    [WebMethod]
    public int Save_Bill_H_Data(string Bill_no, string month, string year, string note , string user) 
    {
        int val = 0;
        try
        {
            string created_date = DateTime.Now.ToString("dd/MM/yy");
            SQL = " INSERT INTO BL_BILL_H ( BILL_NO , MONTH , YEAR , NOTE , CREATED_USER , CRAETED_DATE ) ";
            SQL += " VALUES (  '" + Bill_no + "' ,  '" + month + "' ,  '" + year + "' ,  '" + note + "' ,  '" +user+ "','" + created_date + "' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Bill_H_Data >> ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public string Get_Emp_No_by_Line_No(string Line_No)
    {
        string ret_val = "";

        try
        {
            SQL = " select emp_no from bl_lines where line_no = '"+Line_No+"' and  line_status = '0' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Emp_No_by_Line_No >> To get emp_no");
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_Local_Limit_by_Line_No(string Line_No)
    {
        string ret_val = "";

        try
        {
            SQL = " select APP_limit from bl_lines where line_no = '" + Line_No + "' and  line_status = '0' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Local_Limit_by_Line_No >> To get local limit");
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_Inter_Limit_by_Line_No(string Line_No)
    {
        string ret_val = "";

        try
        {
            SQL = " select Inter_Calls from bl_lines where line_no = '" + Line_No + "' and  line_status = '0' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Inter_Limit_by_Line_No >> To get Inter limit");
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_Local_Flag_by_Line_No(string Line_No)
    {
        string ret_val = "";

        try
        {
            SQL = " select L_Status from bl_lines where line_no = '" + Line_No + "' and  line_status = '0' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Local_Flag_by_Line_No >> To get local flag ");
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_Inter_Flag_by_Line_No(string Line_No)
    {
        string ret_val = "";

        try
        {
            SQL = " select I_Status from bl_lines where line_no = '" + Line_No + "' and  line_status = '0' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Inter_Flag_by_Line_No >> To get inter flag ");
        }

        return ret_val;
    }

    [WebMethod]
    public string Get_Tax_Flag_by_Line_No(string Line_No)
    {
        string ret_val = "";

        try
        {
            SQL = " select tax_flag  from bl_lines where line_no = '" + Line_No + "' and  line_status = '0' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Tax_Flag_by_Line_No >> To get inter flag ");
        }

        return ret_val;
    }

    [WebMethod]
    public double Get_Tax_val_by_Line_No(string Line_No)
    {
        double ret_val = 0;

        try
        {
            SQL = " select Tax_val from bl_lines where line_no = '" + Line_No + "' and  line_status = '0' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = double.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Tax_val_by_Line_No >> To get inter flag ");
        }

        return ret_val;
    }

    [WebMethod]
    public double getFieldByLineNo(string fieldName, string Line_No)
    {
        double ret_val = 0;

        try
        {
            SQL = " select "+fieldName+" from bl_lines where line_no = '" + Line_No + "' and  line_status = '0' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = double.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Sal_Tax_val_by_Line_No >> To get inter flag ");
        }

        return ret_val;
 
    }

    [WebMethod]
    public double Get_Sal_Tax_val_by_Line_No(string Line_No)
    {
        double ret_val = 0;

        try
        {
            SQL = " select Sal_Tax_Per from bl_lines where line_no = '" + Line_No + "' and  line_status = '0' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = double.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Sal_Tax_val_by_Line_No >> To get inter flag ");
        }

        return ret_val;
    }

    [WebMethod]
    public double Get_Tbl_Tax_val_by_Line_No(string Line_No)
    {
        double ret_val = 0;

        try
        {
            SQL = " select Tbl_Tax_Per from bl_lines where line_no = '" + Line_No + "' and  line_status = '0' ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = double.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Tbl_Tax_val_by_Line_No >> To get inter flag ");
        }

        return ret_val;
    }


    public double Get_LI_amounts(string type, string mon_chrgs, string nat_chrgs, string intr_chrgs, string rom_chrgs, string othr_chrgs)
    {
        double ret_val = 0;
        try
        {
            if (type == "L") // Local
            {
                double month_chrgs = double.Parse(mon_chrgs);
                double naton_chrgs = double.Parse(nat_chrgs);
                double other_chrgs = double.Parse(othr_chrgs);

                ret_val =  month_chrgs + naton_chrgs + other_chrgs  ;
            }
            else
            if (type == "I")  // International
            {
                double inter_chrgs = double.Parse(intr_chrgs);
                double room_chrgs = double.Parse(rom_chrgs);

                ret_val = inter_chrgs + room_chrgs   ;
            }
        }
        catch
        { }

        return ret_val ;
    }

    public double Get_Local_Company_Share(string flag, double local, double limit, double taxval, double saltax)
    {
        double ret_val = 0;

        try
        {
            if (flag == "1")
            {
                ret_val = (local * (saltax + 1)) + taxval;
            }
            else
            {
                if ((local * (saltax + 1)) + taxval > limit)
                {
                    ret_val = limit;
                }
                else
                {
                    ret_val = (local * (saltax + 1)) + taxval;
                }
            }

        }
        catch
        {

        }

        return ret_val;
    }

    public double Get_New_Local_Company_Share(string flag, double local, double limit, double taxval, double saltax, double newSalTax, double tblTax , string tax_flag )
    {
        double ret_val = 0;

        try
        {
            //if (local != 0 )
            //{
            if (tax_flag.Trim() == "1")  // calculate tax *************************
            {
                if (flag == "1")
                {
                    ret_val = ((local * (tblTax + 1)) * (newSalTax + 1)) + taxval;
                }
                else
                {
                    if (((local * (tblTax + 1)) * (newSalTax + 1)) + taxval > limit)
                    {
                        ret_val = limit;
                    }
                    else
                    {
                        ret_val = ((local * (tblTax + 1)) * (newSalTax + 1)) + taxval;
                    }
                }
            }
            else // donot calculate tax
            {
                if (flag == "1")
                {
                    ret_val =  local  ;
                }
                else
                {
                    if ( local > limit)
                    {
                        ret_val = limit;
                    }
                    else
                    {
                        ret_val =  local  ;
                    }
                }
            }

            //}
            //else
            //{
            //    ret_val = 0;
            //}
        }
        catch
        {

        }

        return ret_val;
    }

    public double Get_Local_Employee_Share(string flag, double local, double limit, double taxval, double saltax)
    {
        double ret_val = 0;

        try
        {
            if (flag == "1")
            {
                ret_val = 0;
            }
            else
            {
                if ((local * (saltax + 1)) + taxval > limit)
                {
                    ret_val = ((local * (saltax + 1)) + taxval) - limit;
                }
                else
                {
                    ret_val = 0;
                }
            }

        }
        catch
        {

        }

        return ret_val;
    }

    [WebMethod]
    public double Get_New_Local_Employee_Share(string flag, double local, double limit, double taxval, double saltax, double newSalTax, double tblTax, string tax_flag )
    {
        double ret_val = 0;

        try
        {
            //if (local != 0)
            //{
                if (tax_flag.Trim() == "1")  // calculate tax
                {
                    if (flag == "1")
                    {
                        ret_val = 0;
                    }
                    else
                    {

                        if (((local * (tblTax + 1)) * (newSalTax + 1)) + taxval > limit)
                        {
                            ret_val = (((local * (tblTax + 1)) * (newSalTax + 1)) + taxval) - limit;
                        }
                        else
                        {
                            ret_val = 0;
                        }
                    }
                }
                else // donot calculate tax
                {
                    if (flag == "1")
                    {
                        ret_val = 0;
                    }
                    else
                    {

                        if (local > limit)
                        {
                            ret_val = local - limit;
                        }
                        else
                        {
                            ret_val = 0;
                        }
                    }
                }
            //}
            //else
            //{
            //    ret_val = 0;
            //}
        }
        catch
        {

        }

        return ret_val;
    }

    public double Get_New_Inter_Company_Share(string flag, double inter, double limit, double taxval, double saltax, double newSalTax, double tblTax , string tax_flag )
    {
        double ret_val = 0;

        try
        {
            //if (inter != 0)
            //{
                if (tax_flag.Trim() == "1") // calculate tax
                {
                    if (flag == "1")
                    {

                        ret_val = ((inter * (tblTax + 1)) * (newSalTax + 1));
                    }
                    else
                    {
                        if ((((inter * (tblTax + 1)) * (newSalTax + 1))) > limit)
                        {
                            ret_val = limit;
                        }
                        else
                        {
                            ret_val = ((inter * (tblTax + 1)) * (newSalTax + 1));
                        }
                    }
                }
                else  // do not calculate tax
                {
                    if (flag == "1")
                    {

                        ret_val = inter;
                    }
                    else
                    {
                        if (inter > limit)
                        {
                            ret_val = limit;
                        }
                        else
                        {
                            ret_val = inter;
                        }
                    }
                }
            //}
            //else
            //{
            //    ret_val = 0;
            //}
        }
        catch
        {

        }

        return ret_val;
    }
    public double Get_Inter_Company_Share(string flag, double inter, double limit, double taxval, double saltax)
    {
        double ret_val = 0;

        try
        {
            if (flag == "1")
            {
                ret_val = inter * (saltax + 1);
            }
            else
            {
                if ((inter * (saltax + 1)) > limit)
                {
                    ret_val = limit;
                }
                else
                {
                    ret_val = inter * (saltax + 1);
                }
            }

        }
        catch
        {

        }

        return ret_val;
    }

    public double Get_New_Inter_Employee_Share(string flag, double inter, double limit, double taxval, double saltax, double newSalTax, double tblTax , string tax_flag )
    {
        double ret_val = 0;

        try
        {
            //if (inter != 0)
            //{
                if (tax_flag.Trim() == "1") // calculate tax
                {
                    if (flag == "1")
                    {
                        ret_val = 0;
                    }
                    else
                    {

                        if ((((inter * (tblTax + 1)) * (newSalTax + 1))) > limit)
                        {
                            ret_val = (((inter * (tblTax + 1)) * (newSalTax + 1))) - limit;
                        }
                        else
                        {
                            ret_val = 0;
                        }
                    }
                }
                else  // do not calculate tax
                {
                    if (flag == "1")
                    {
                        ret_val = 0;
                    }
                    else
                    {

                        if (inter > limit)
                        {
                            ret_val = inter - limit;
                        }
                        else
                        {
                            ret_val = 0;
                        }
                    }
                }
            //}
            //else
            //{
            //    ret_val = 0;
            //}
        }
        catch
        {

        }

        return ret_val;
    }
    public double Get_Inter_Employee_Share(string flag, double inter, double limit, double taxval, double saltax)
    {
        double ret_val = 0;

        try
        {
            if (flag == "1")
            {
                ret_val = 0;
            }
            else
            {
                if ((inter * (saltax + 1)) > limit)
                {
                    ret_val = (inter * (saltax + 1)) - limit;
                }
                else
                {
                    ret_val = 0;
                }
            }

        }
        catch
        {

        }

        return ret_val;
    }



    [WebMethod]
    public int Save_Bill_D_Data(string Bill_no, string line_no , string rate_plan , string mon_chrgs , string nat_chrgs , string intr_chrgs , string rom_chrgs , string othr_chrgs )
    {
        int val = 0;
        try
        {
            double Total = 0 ;
            double newTotal = 0 ;

            string Emp_no = Get_Emp_No_by_Line_No(line_no).ToString();

            string Local_limit = Get_Local_Limit_by_Line_No(line_no).ToString();
            string Inter_limit = Get_Inter_Limit_by_Line_No(line_no).ToString();

            string Local_flag = Get_Local_Flag_by_Line_No(line_no).ToString();
            string Inter_falg = Get_Inter_Flag_by_Line_No(line_no).ToString();

            double local_val = Get_LI_amounts("L", mon_chrgs  ,  nat_chrgs ,  intr_chrgs , rom_chrgs  ,  othr_chrgs );
            double Inter_val = Get_LI_amounts("I", mon_chrgs, nat_chrgs, intr_chrgs, rom_chrgs, othr_chrgs);
            double Tot_before_tax = local_val + Inter_val;

            string tax_flag = Get_Tax_Flag_by_Line_No(line_no);
            double tax_val = Get_Tax_val_by_Line_No(line_no);

            double sal_tax = getFieldByLineNo("sal_tax_per",line_no);
            double new_sal_tax = getFieldByLineNo("new_sales_tax_per",line_no);
            double tbl_tax = getFieldByLineNo("tbl_tax_per",line_no);

            //if (Tot_before_tax != 0)
            //{
                if (tax_flag.Trim() == "1")
                {
                    //old total: double Total = ((local_val + Inter_val) * (sal_tax + 1)) + tax_val;
                    Total = ((local_val + Inter_val) * (sal_tax + 1)) + tax_val;
                    newTotal = (((local_val + Inter_val) * (tbl_tax + 1)) * (new_sal_tax + 1)) + tax_val;
                    // double tax_diff = (newTotal - Total);
                    //  double newCost = (tax_diff / 2);
                }
                else
                {
                    Total = local_val + Inter_val;
                    newTotal = local_val + Inter_val;
                    // double tax_diff =  newTotal - Total ;
                    // double newCost = (tax_diff / 2);
                }
            //}
            //else
            //{
            //    Total =  0 ;
            //    newTotal = 0 ;
            //}

           // double LCS = Get_Local_Company_Share(Local_flag, local_val, double.Parse(Local_limit), tax_val, sal_tax);
            double NLCS = Get_New_Local_Company_Share(Local_flag, local_val, double.Parse(Local_limit), tax_val, sal_tax, new_sal_tax, tbl_tax, tax_flag);
           // double LES = Get_Local_Employee_Share(Local_flag, local_val, double.Parse(Local_limit), tax_val, sal_tax );
            double NLES = Get_New_Local_Employee_Share(Local_flag, local_val, double.Parse(Local_limit), tax_val, sal_tax, new_sal_tax, tbl_tax, tax_flag);

            //double ICS = Get_Inter_Company_Share(Inter_falg, Inter_val, double.Parse(Inter_limit), tax_val, sal_tax);
            double NICS = Get_New_Inter_Company_Share(Inter_falg, Inter_val, double.Parse(Inter_limit), tax_val, sal_tax, new_sal_tax, tbl_tax, tax_flag);
            //double IES = Get_Inter_Employee_Share(Inter_falg, Inter_val, double.Parse(Inter_limit), tax_val, sal_tax);
            double NIES = Get_New_Inter_Employee_Share(Inter_falg, Inter_val, double.Parse(Inter_limit), tax_val, sal_tax, new_sal_tax, tbl_tax, tax_flag);  

            double Company_Share = NLCS + NICS ;
            double Employee_Share = NLES + NIES ;

            int flag = UPDATE_Line_Rate_Plan(line_no, Emp_no, rate_plan);

            SQL = " INSERT INTO BL_BILL_D ( BILL_NO , LINE_NO , RATE_PLAN , MON_CHRGS , NAT_CHRGS "+
                  ", INTR_CHRGS , ROM_CHRGS , OTHR_CHRGS , EMP_NO , Local_limit "+
                  ", inter_limit , local_flag , inter_flag , local , inter "+
                  ", total , comp_share , emp_share , tax_val , LCS "+
                  ", ICS , LES , IES  ) ";


            SQL += " VALUES (  '" + Bill_no + "' ,  '" + line_no + "' ,  '" + rate_plan + "' ,  '" + mon_chrgs + "' ,  '" + nat_chrgs +
                   "','" + intr_chrgs + "','" + rom_chrgs + "','" + othr_chrgs + "','" + Emp_no + "','" + Local_limit + 
                   "' ,'" + Inter_limit + "','" + Local_flag + "','" + Inter_falg + "','" + local_val.ToString() + "','" + Inter_val.ToString() +
                   "','" + newTotal.ToString() + "','" + Company_Share + "','" + Employee_Share + "','" + tax_val.ToString() + "' ,'" + NLCS.ToString() + 
                   "','" + NICS.ToString() + "','" + NLES.ToString() + "','" + NIES.ToString() + "' ) ";


            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Save_Bill_D_Data >> ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int UPDATE_Line_Rate_Plan(string Line_no , string emp_no , string rate_plan )
    {
        int val = 0;
        try
        {

            SQL = " UPDATE BL_Lines  SET RATE_PLAN = '"+rate_plan+"'  WHERE Line_NO = '" + Line_no + "' and Emp_no = '"+emp_no+"' and line_status = '0'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "UPDATE_Line_Rate_Plan >> ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int UPDATE_Bill_H_Data(string Bill_no , string month , string year , string note)
    {
        int val = 0;
        try
        {

            SQL = " UPDATE BL_BILL_H SET MONTH = '" + month + "',YEAR = '" + year + "' ,NOTE = '" + note + "'  WHERE BILL_NO = '" + Bill_no + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "DELETE_Bill_D_Data >> ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int DELETE_Bill_H_Data(string Bill_no)
    {
        int val = 0;
        try
        {
            
            SQL = " DELETE FROM BL_BILL_H WHERE BILL_NO = '"+Bill_no+"' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "DELETE_Bill_H_Data >> ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int DELETE_Bill_D_Data(string Bill_no)
    {
        int val = 0;
        try
        {

            SQL = " DELETE FROM BL_BILL_D WHERE BILL_NO = '" + Bill_no + "' ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "DELETE_Bill_D_Data >> ");
            val = 0;
        }

        return val;
    }

    #endregion
    //**********************************************************************
    #region " Reports Data "

    [WebMethod]
    public DataSet Get_Bill_BY_BILL_NO(string Bill_No)
    {
        try
        {
           
            
            //*** old
            //SQL = "    SELECT C.YEAR , C.MONTH , C.BILL_NO , A.EMP_NO , D.emp_name ,D.SECTION_NAME , D.VAL_ADESC , A.line_no , B.app_limit , B.INTER_CALLS as INTR_CALS ,  ((A.MON_CHRGS + A.NAT_CHRGS + A.INTR_CHRGS + A.ROM_CHRGS + A.OTHR_CHRGS ) * (B.SAL_TAX_PER + 1) ) + B.TAX_VAL    as AFTR_SAL_TAX , ( A.INTR_CHRGS + A.ROM_CHRGS ) as Intrnational ,   ( A.MON_CHRGS + A.NAT_CHRGS + A.OTHR_CHRGS ) as B_LOCAL ,         CASE    WHEN B.L_STATUS = '1'   THEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS) * ( B.SAL_TAX_PER + 1) ) + B.TAX_VAL    WHEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS) * (B.SAL_TAX_PER + 1 ) ) + B.TAX_VAL   > B.APP_LIMIT    THEN  B.APP_LIMIT   ELSE    ((A.MON_CHRGS + A.NAT_CHRGS + A.OTHR_CHRGS) * ( B.SAL_TAX_PER + 1) ) + B.TAX_VAL       END as LOCAL_COMP_SHAR ,      CASE    WHEN B.L_STATUS = '1'   THEN   0   WHEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS)  * (B.SAL_TAX_PER + 1) ) + B.TAX_VAL     > B.APP_LIMIT   THEN (( (A.MON_CHRGS + A.NAT_CHRGS +  A.OTHR_CHRGS ) * (B.SAL_TAX_PER + 1) ) + B.TAX_VAL )  - B.APP_LIMIT    ELSE  0  END as LOCAL_EMP_SHAR , CASE    WHEN B.I_STATUS = '1'    THEN  ( ( A.INTR_CHRGS + A.ROM_CHRGS ) * (B.SAL_TAX_PER + 1) )       WHEN  ((A.INTR_CHRGS + A.ROM_CHRGS )  * (B.SAL_TAX_PER + 1) )  > B.INTER_CALLS      THEN  B.INTER_CALLS        ELSE   (A.INTR_CHRGS + A.ROM_CHRGS) * ( B.SAL_TAX_PER + 1)       END as INTER_COMP_SHAR ,   CASE  WHEN B.I_STATUS = '1'   THEN 0  WHEN   (A.INTR_CHRGS + A.ROM_CHRGS)  * ( B.SAL_TAX_PER + 1 )  > B.INTER_CALLS   THEN  (( A.INTR_CHRGS + A.ROM_CHRGS )  * ( B.SAL_TAX_PER + 1) )    - B.INTER_CALLS ELSE    0          END as INTER_EMP_SHAR , B.TAX_VAL      ";
            //SQL += "  From BL_BILL_H C, BL_BILL_D A , BL_LINES  B , PAY_EGIC.Bil_Emp D         where A.BILL_NO = C.BILL_NO  and B.LINE_NO = A.LINE_NO  AND A.EMP_NO = D.EMP_NO  and C.BILL_NO   = '" + Bill_No + "'   and ( to_date('01-'||c.month||'-'||c.year,'dd-mm-yyyy') <= b.dd_date or b.dd_date is null  ) ";  // and b.emp_no = a.emp_no


            SQL = " SELECT C.YEAR , C.MONTH , C.BILL_NO , A.EMP_NO , D.emp_name ," +
                    "D.SECTION_NAME , D.VAL_ADESC , A.line_no , B.app_limit , B.INTER_CALLS as INTR_CALS ," +
                    "  A.TOTAL as AFTR_SAL_TAX ," +
                    " A.INTER as Intrnational ,   A.LOCAL as B_LOCAL , " +
                    " A.LCS as LOCAL_COMP_SHAR ,  " +
                    " A.LES as LOCAL_EMP_SHAR , A.ICS as INTER_COMP_SHAR , A.IES as INTER_EMP_SHAR , " +
                    " B.TAX_VAL , A.TAX_DIFF, A.NLCS, A.NICS, A.NLES, A.NIES, A.NEW_TOTAL  , B.LINE_TYPE  ";
            SQL += "  From BL_BILL_H C, BL_BILL_D A , BL_LINES  B , PAY_EGIC.Bil_Emp D  " +
                    "  where A.BILL_NO = C.BILL_NO  and B.LINE_NO = A.LINE_NO  AND A.EMP_NO = D.EMP_NO  " +
                    "  and c.bill_no = '"+Bill_No+"' and " +
                    "  ( to_date('01-'||c.month||'-'||c.year,'dd-mm-yyyy') <= b.dd_date or b.dd_date is null  )  ";


            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Bill_BY_BILL_NO ");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Bill_BY_EMP( string EMP_NO , string FMONTH, string TMONTH, string FYEAR, string TYEAR)
    {
        try
        {

            //SQL = " SELECT C.YEAR , C.MONTH , C.BILL_NO , A.EMP_NO , D.emp_name ,D.SECTION_NAME , "+
            //    " D.VAL_ADESC , A.line_no , B.app_limit , B.INTER_CALLS as INTR_CALS , "+
            //    " ((A.MON_CHRGS + A.NAT_CHRGS + A.INTR_CHRGS + A.ROM_CHRGS + A.OTHR_CHRGS ) * (B.SAL_TAX_PER + 1) ) + B.TAX_VAL    as AFTR_SAL_TAX , "+
            //    " ( A.INTR_CHRGS + A.ROM_CHRGS ) as Intrnational ,   ( A.MON_CHRGS + A.NAT_CHRGS + A.OTHR_CHRGS ) as B_LOCAL , "+
            //    " CASE    WHEN B.L_STATUS = '1'   THEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS) * ( B.SAL_TAX_PER + 1) ) + B.TAX_VAL "+
            //    " WHEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS) * (B.SAL_TAX_PER + 1 ) ) + B.TAX_VAL   > B.APP_LIMIT "+
            //    " THEN  B.APP_LIMIT   ELSE    ((A.MON_CHRGS + A.NAT_CHRGS + A.OTHR_CHRGS) * ( B.SAL_TAX_PER + 1) ) + B.TAX_VAL  "+
            //    " END as LOCAL_COMP_SHAR ,      CASE    WHEN B.L_STATUS = '1'   THEN   0   "+
            //    " WHEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS)  * (B.SAL_TAX_PER + 1) ) + B.TAX_VAL     > B.APP_LIMIT   "+
            //    " THEN (( (A.MON_CHRGS + A.NAT_CHRGS +  A.OTHR_CHRGS ) * (B.SAL_TAX_PER + 1) ) + B.TAX_VAL )  - B.APP_LIMIT    "+
            //    " ELSE  0  END as LOCAL_EMP_SHAR , CASE    WHEN B.I_STATUS = '1'    THEN  ( ( A.INTR_CHRGS + A.ROM_CHRGS ) * (B.SAL_TAX_PER + 1) )   "+
            //    " WHEN  ((A.INTR_CHRGS + A.ROM_CHRGS )  * (B.SAL_TAX_PER + 1) )  > B.INTER_CALLS      THEN  B.INTER_CALLS   "+
            //    " ELSE   (A.INTR_CHRGS + A.ROM_CHRGS) * ( B.SAL_TAX_PER + 1)       END as INTER_COMP_SHAR ,   CASE  WHEN B.I_STATUS = '1' "+
            //    " THEN 0  WHEN   (A.INTR_CHRGS + A.ROM_CHRGS)  * ( B.SAL_TAX_PER + 1 )  > B.INTER_CALLS   THEN "+
            //    " (( A.INTR_CHRGS + A.ROM_CHRGS )  * ( B.SAL_TAX_PER + 1) )    - B.INTER_CALLS ELSE    0          END as INTER_EMP_SHAR , B.TAX_VAL,A.NLCS, A.NICS, A.NLES, A.NIES, A.NEW_TOTAL      ";
            //SQL += "  From BL_BILL_H C, BL_BILL_D A , BL_LINES  B , PAY_EGIC.Bil_Emp D "+
            //    " where A.BILL_NO = C.BILL_NO  and B.LINE_NO = A.LINE_NO   AND A.EMP_NO = D.EMP_NO  and C.MONTH between '" +
            //    FMONTH + "' and '" + TMONTH + "'  AND  C.YEAR between '" + FYEAR + "' and '" + TYEAR + "'  and B.EMP_NO = '" + EMP_NO +
            //    "' and ( to_date('01-'||c.month||'-'||c.year,'dd-mm-yyyy') <= b.dd_date or b.dd_date is null  )  "; // and b.emp_no = a.emp_no

            //******************************************************************************
            SQL = " SELECT C.YEAR , C.MONTH , C.BILL_NO , A.EMP_NO , D.emp_name ," +
                    "D.SECTION_NAME , D.VAL_ADESC , A.line_no , B.app_limit , B.INTER_CALLS as INTR_CALS ," +
                    "  A.TOTAL as AFTR_SAL_TAX ," +
                    " A.INTER as Intrnational ,   A.LOCAL as B_LOCAL , " +
                    " A.LCS as LOCAL_COMP_SHAR ,  " +
                    " A.LES as LOCAL_EMP_SHAR , A.ICS as INTER_COMP_SHAR , A.IES as INTER_EMP_SHAR , " +
                    " B.TAX_VAL , A.TAX_DIFF, A.NLCS, A.NICS, A.NLES, A.NIES, A.NEW_TOTAL  , B.LINE_TYPE ";
            SQL += "  From BL_BILL_H C, BL_BILL_D A , BL_LINES  B , PAY_EGIC.Bil_Emp D  " +
                    "  where A.BILL_NO = C.BILL_NO  and B.LINE_NO = A.LINE_NO  AND A.EMP_NO = D.EMP_NO  " +
                    "  and C.MONTH between '" + FMONTH + "' AND  '" + TMONTH + "'  AND  C.YEAR  BETWEEN '" + FYEAR + "' and  '" + TYEAR + "' AND B.EMP_NO = '" + EMP_NO +"' AND  " +
                    "  ( to_date('01-'||c.month||'-'||c.year,'dd-mm-yyyy') <= b.dd_date or b.dd_date is null  )  ";
           
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Bill_BY_EMP ");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Bill_BY_DATE(string MONTH , string YEAR)
    {
        try
        {
            //OLD QUERY
            //SQL = " SELECT C.YEAR , C.MONTH , C.BILL_NO , A.EMP_NO , D.emp_name ," +
            //    "D.SECTION_NAME , D.VAL_ADESC , A.line_no , B.app_limit , B.INTER_CALLS as INTR_CALS ," +
            //    "  ((A.MON_CHRGS + A.NAT_CHRGS + A.INTR_CHRGS + A.ROM_CHRGS + A.OTHR_CHRGS ) * (B.SAL_TAX_PER + 1) ) + B.TAX_VAL    as AFTR_SAL_TAX ," +
            //    " ( A.INTR_CHRGS + A.ROM_CHRGS ) as Intrnational ,   ( A.MON_CHRGS + A.NAT_CHRGS + A.OTHR_CHRGS ) as B_LOCAL , " +
            //    " CASE    WHEN B.L_STATUS = '1'   THEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS) * ( B.SAL_TAX_PER + 1) ) + B.TAX_VAL   " +
            //    " WHEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS) * (B.SAL_TAX_PER + 1 ) ) + B.TAX_VAL   > B.APP_LIMIT    THEN  B.APP_LIMIT  " +
            //    " ELSE    ((A.MON_CHRGS + A.NAT_CHRGS + A.OTHR_CHRGS) * ( B.SAL_TAX_PER + 1) ) + B.TAX_VAL END as LOCAL_COMP_SHAR ,  " +
            //    " CASE    WHEN B.L_STATUS = '1'   " +
            //    " THEN   0   WHEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS)  * (B.SAL_TAX_PER + 1) ) + B.TAX_VAL > B.APP_LIMIT " +
            //    "  THEN (( (A.MON_CHRGS + A.NAT_CHRGS +  A.OTHR_CHRGS ) * (B.SAL_TAX_PER + 1) ) + B.TAX_VAL )  - B.APP_LIMIT  " +
            //    "  ELSE  0  END as LOCAL_EMP_SHAR , CASE    WHEN B.I_STATUS = '1'    THEN  ( ( A.INTR_CHRGS + A.ROM_CHRGS ) * (B.SAL_TAX_PER + 1) )   " +
            //    "    WHEN  ((A.INTR_CHRGS + A.ROM_CHRGS )  * (B.SAL_TAX_PER + 1) )  > B.INTER_CALLS      THEN  B.INTER_CALLS    " +
            //    "    ELSE   (A.INTR_CHRGS + A.ROM_CHRGS) * ( B.SAL_TAX_PER + 1) END as INTER_COMP_SHAR , CASE  WHEN B.I_STATUS = '1'  " +
            //    " THEN 0  WHEN   (A.INTR_CHRGS + A.ROM_CHRGS)  * ( B.SAL_TAX_PER + 1 )  > B.INTER_CALLS  " +
            //    " THEN  (( A.INTR_CHRGS + A.ROM_CHRGS )  * ( B.SAL_TAX_PER + 1) )    - B.INTER_CALLS ELSE    0  END as INTER_EMP_SHAR , " +
            //    // add here new fileds and add it to roi 
            //    " B.TAX_VAL , A.TAX_DIFF, A.NLCS, A.NICS, A.NLES, A.NIES    ";



                    SQL =  " SELECT C.YEAR , C.MONTH , C.BILL_NO , A.EMP_NO , D.emp_name ,"+
                            "D.SECTION_NAME , D.VAL_ADESC , A.line_no , B.app_limit , B.INTER_CALLS as INTR_CALS ,"+
                            "  A.TOTAL as AFTR_SAL_TAX ,"+
                            " A.INTER as Intrnational ,   A.LOCAL as B_LOCAL , "+
                            " A.LCS as LOCAL_COMP_SHAR ,  "+
                            " A.LES as LOCAL_EMP_SHAR , A.ICS as INTER_COMP_SHAR , A.IES as INTER_EMP_SHAR , "+
                            " B.TAX_VAL , A.TAX_DIFF, A.NLCS, A.NICS, A.NLES, A.NIES, A.NEW_TOTAL  , B.LINE_TYPE ";
                    SQL += "  From BL_BILL_H C, BL_BILL_D A , BL_LINES  B , PAY_EGIC.Bil_Emp D  "+
                            "  where A.BILL_NO = C.BILL_NO  and B.LINE_NO = A.LINE_NO  AND A.EMP_NO = D.EMP_NO  "+
                            "  and C.MONTH = '" + MONTH + "' AND  C.YEAR = '" + YEAR + "' and "+
                            "  ( to_date('01-'||c.month||'-'||c.year,'dd-mm-yyyy') <= b.dd_date or b.dd_date is null  )  ";


           // SQL += " From BL_BILL_H C, BL_BILL_D A , BL_LINES  B , PAY_EGIC.Bil_Emp D    where A.BILL_NO = C.BILL_NO  and B.LINE_NO = A.LINE_NO  AND A.EMP_NO = D.EMP_NO      and C.MONTH = '" + MONTH + "' AND  C.YEAR = '" + YEAR + "' and ( to_date('01-'||c.month||'-'||c.year,'dd-mm-yyyy') <= b.dd_date or b.dd_date is null  )  ";

            //*** old
          //  SQL = " SELECT C.YEAR , C.MONTH , C.BILL_NO , A.EMP_NO , D.emp_name ,D.SECTION_NAME , D.VAL_ADESC , A.line_no , B.app_limit , B.INTER_CALLS as INTR_CALS ,  ((A.MON_CHRGS + A.NAT_CHRGS + A.INTR_CHRGS + A.ROM_CHRGS + A.OTHR_CHRGS ) * 1.15 )   as AFTR_SAL_TAX ,  ( A.INTR_CHRGS + A.ROM_CHRGS ) as Intrnational , ( A.MON_CHRGS + A.NAT_CHRGS + A.OTHR_CHRGS ) as B_LOCAL ,   CASE    WHEN B.L_STATUS = '1' THEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS) * 1.15 )   ";
          //  SQL += " WHEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS)  * 1.15 )   > B.APP_LIMIT THEN  B.APP_LIMIT   ELSE   ((A.MON_CHRGS + A.NAT_CHRGS + A.OTHR_CHRGS) * 1.15 )     END as LOCAL_COMP_SHAR ,  CASE   WHEN B.L_STATUS = '1' THEN   0  WHEN ((A.MON_CHRGS+ A.NAT_CHRGS + A.OTHR_CHRGS)  * 1.15 )   > B.APP_LIMIT  THEN (( A.MON_CHRGS + A.NAT_CHRGS +  A.OTHR_CHRGS ) * 1.15 )   - B.APP_LIMIT     ELSE  0  END as LOCAL_EMP_SHAR ,        ";
          //  SQL += " CASE    WHEN B.I_STATUS = '1'  THEN  ( ( A.INTR_CHRGS + A.ROM_CHRGS ) * 1.15 )     WHEN ( (A.INTR_CHRGS + A.ROM_CHRGS )  * 1.15 )   > B.INTER_CALLS    THEN  ( B.INTER_CALLS  * 1.15 )      ELSE         ( (A.INTR_CHRGS + A.ROM_CHRGS) * 1.15 )     END as INTER_COMP_SHAR , CASE  WHEN B.I_STATUS = '1'  THEN 0  WHEN  ( (A.INTR_CHRGS + A.ROM_CHRGS)  * 1.15 )   > B.INTER_CALLS  THEN ( (( A.INTR_CHRGS + A.ROM_CHRGS ) - B.INTER_CALLS ) * 1.15 )                 ELSE    0       END as INTER_EMP_SHAR , B.TAX_VAL   from BL_BILL_H C, BL_BILL_D A , BL_LINES  B , PAY_EGIC.Bil_Emp D    ";
         //   SQL += "  where A.BILL_NO = C.BILL_NO  and B.LINE_NO = A.LINE_NO  AND A.EMP_NO = D.EMP_NO      and C.MONTH = '" + MONTH + "' AND  C.YEAR = '" + YEAR + "' and ( to_date('01-'||c.month||'-'||c.year,'dd-mm-yyyy') <= b.dd_date or b.dd_date is null  )  ";
            //**** + 0.51
           

            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL); 
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Bill_BY_BILL_NO ");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Lines(string  employee , string department,string status, string lineType , string finished )
    {
        try
        {

            SQL = "  select  bl_lines.line_no , BIL_EMP.emp_no ,BIL_EMP.emp_name, BIL_EMP.SECTION_NAME , BIL_EMP.SECTION_NO , bl_lines.app_limit , bl_lines.inter_calls ,   case When bl_lines.L_STATUS = '0' then 'Closed' else 'Open' end as L_STATUS ,  case when bl_lines.I_STATUS = '0' then 'Closed' else 'Open' end as I_STATUS , case when bl_lines.line_status = '0' then 'Running' else 'Stopped' end as line_status  ,     bl_lines.rate_plan , bl_lines.line_type      from pay_egic.BIL_EMP , BL_LINES  ";
            SQL += " where  BIL_EMP.emp_no = bl_lines.emp_no And  ( BIL_EMP.emp_no = '" + employee + "' OR '" + employee + "' = '0' )  And  ( BIL_EMP.SECTION_NO = '" + department + "' OR  '" + department + "' = '0' )  And  bl_lines.line_STATUS = '" + status + "'  And ( bl_lines.line_type = '" + lineType + "' OR  '" + lineType + "' = '0') and ( BIL_EMP.Finshed = '" + finished + "' ) order by 1   ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Lines ");
        }
        return Ds;
    }

    [WebMethod]
    public string Get_Count_Duplicate_Employee()
    {
         string count = "0" ;
        try
        {
           
            SQL = " select count(*) from pay_egic.OS_EMP where pay_egic.OS_EMP.emp_no in ( SELECT EMP_NO FROM pay_egic.EMP )  ";
            
            count = oracleacess.ExecuteScalar(SQL).ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Count_Duplicate_Employee ");
        }
        return count ;
    }


    [WebMethod]
    public DataSet Get_Duplicate_Employee()
    {
        try
        {

            SQL = " select EMP_No , EMP_NAME from pay_egic.OS_EMP where pay_egic.OS_EMP.emp_no in ( SELECT EMP_NO FROM pay_egic.EMP )  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Duplicate_Employee ");
        }
        return Ds;
    }

    [WebMethod]
    public DataSet Get_Duplicate_Lines()
    {
        try
        {

            SQL = " select * from ( select line_no , count(line_no) as cont , line_status from bl_lines A group by line_no , line_status )  where cont > 1 and line_status = '0'  ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_Duplicate_Lines ");
        }
        return Ds;
    }

    //[WebMethod]
    //public DataSet Get_Bill_New()
    //{
    //    try
    //    {

    //        SQL = "   ";
    //        SQL += "  ";
    //        Ds.Clear();
    //        dt = oracleacess.ExecuteDataTable(SQL);
    //        Ds.Tables.Add(dt);
    //    }
    //    catch (Exception ex)
    //    {
    //        LogError(ex.Message, "Get_Buplicate_Employee ");
    //    }
    //    return Ds;
    //}

    #endregion
    //**********************************************************************
    #region " Check Data "

    [WebMethod]
    public string Check_Exist_Line( string line_no )
    {
        string ret_val = "";

        try
        {
            SQL = " select line_no from bl_lines where line_no = '" + line_no + "' and line_status = '0'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            if (Pass.ToString() == "")
            {
                ret_val = "0";
            }
            else
            {
                ret_val = "1";
            }
        }
        catch (Exception ex)
        {
            //LogError(ex.Message, "Check_Exist_Line  ");
            ret_val = "0";
        }

        return ret_val;
    }

    [WebMethod]
    public int Delete_NOT_Saved_Lines( string line_no )
    {
        int val = 0;
        try
        {
            SQL = " DELETE FROM BL_NOT_SAVED_LINES WHERE LINE_NO = '"+line_no+"'  ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Delete_NOT_Saved_Lines >> ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public int Save_NOT_Saved_Lines(string Line_no , string Rate_paln )
    {
      
        int val = 0;
        try
        {
            SQL = " INSERT INTO BL_NOT_SAVED_LINES ( LINE_NO , RATE_PLAN ) VALUES ( '" + Line_no + "','"+Rate_paln+"' ) ";
            oracleacess.ExecuteNonQuery(SQL);
            val = 1;
        }
        catch (Exception ex)
        {
            //LogError(ex.Message, "Save_NOT_Saved_Lines >> ");
            val = 0;
        }

        return val;
    }

    [WebMethod]
    public string Get_not_saved_count()
    {
        string ret_val = "";

        try
        {
            SQL = " select count(line_no) from bl_not_saved_lines ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = Pass.ToString();
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Get_not_saved_count >> To get User Name");
        }

        return ret_val;
    }

    [WebMethod]
    public DataSet Bind_Not_Saved_Lines_GRID()
    {
        try
        {
            SQL = " select Line_No , Rate_Plan from BL_NOT_SAVED_LINES ";
            Ds.Clear();
            dt = oracleacess.ExecuteDataTable(SQL);
            Ds.Tables.Add(dt);
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Bind_Not_Saved_Lines_GRID ");
        }
        return Ds;
    }

    [WebMethod]
    public int Check_Duplicate_Line_No_In_bill(string Line_No)
    {
        int ret_val = 0;

        try
        {
            SQL = " select cont from ( select line_no , count(line_no) as cont , line_status from bl_lines A group by line_no , line_status )  where line_status = '0' and line_no = '"+Line_No+"'  ";
            object Pass = oracleacess.ExecuteScalar(SQL);
            ret_val = int.Parse(Pass.ToString());
        }
        catch (Exception ex)
        {
            LogError(ex.Message, "Check_Duplicate_Line_No_In_bill >>  ");
        }

        return ret_val;
    }

    #endregion
    //**********************************************************************
}