﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Data;
using System.Data.OracleClient;
using System.Data.SqlClient;
using System.Configuration;

/// <summary>
/// Summary description for OracleAcess
/// </summary>
public class OracleAcess
{
    private string connectionstring;
    public OracleConnection oracc;

    private bool WithTransaction;
    private OracleTransaction _transaction;

    public OracleAcess(string ConnectionName)
    {
        WithTransaction = false;
        if (!string.IsNullOrEmpty(ConnectionName))
        {
            //name provided so search for that connection
            connectionstring = ConfigurationManager.ConnectionStrings["" + ConnectionName + ""].ConnectionString;
            //connectionstring = ConfigurationManager.AppSettings.Get(ConnectionName).ToString();
        }
        //return the value    
        oracc = new OracleConnection(connectionstring);

    }

    //public string GetUserName()
    //{
    //    string str = cnn.ConnectionString.ToLower;
    //    //Data Source=.;Initial Catalog=Dat_50_T;Persist Security Info=True;User ID=sa;Password=ilhsmms 
    //    return str.Substring(str.IndexOf("user id=")).Remove(0, 8).Split(";")(0);
    //}
    //public object GetPassword()
    //{
    //    string str = cnn.ConnectionString.ToLower;
    //    return str.Substring(str.IndexOf("password=")).Remove(0, 9).Split(";")(0);
    //}

    public OracleTransaction Transaction
    {
        get { return this._transaction; }
        set { this._transaction = value; }
    }
    public int ExecuteNonQuery(string sql)
    {
        OracleCommand cmd = new OracleCommand(sql, oracc);
        if (oracc.State != ConnectionState.Open)
        {
            oracc.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        int retval = cmd.ExecuteNonQuery();
        if ((!WithTransaction))
        {
            oracc.Close();
        }
        return retval;
    }
    public int ExecuteNonQuery(string sql, OracleParameter[] @params)
    {
        OracleCommand cmd = new OracleCommand(sql, oracc);
        for (int i = 0; i <= @params.Length - 1; i++)
        {
            cmd.Parameters.Add(@params[i]);
        }

        if (oracc.State != ConnectionState.Open)
        {
            oracc.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        int retval = cmd.ExecuteNonQuery();
        if ((!WithTransaction))
        {
            oracc.Close();
        }
        return retval;
    }
    public void SaveData(DataTable dt, string Sql)
    {
        OracleDataAdapter Adp = new OracleDataAdapter(Sql, oracc);
        OracleCommandBuilder CmAdp = new OracleCommandBuilder(Adp);

        Adp.InsertCommand = CmAdp.GetInsertCommand();
        Adp.DeleteCommand = CmAdp.GetDeleteCommand();
        Adp.UpdateCommand = CmAdp.GetUpdateCommand();
        Adp.Update(dt);
    }
    public void BeginTransaction()
    {
        WithTransaction = true;
        if (oracc.State != ConnectionState.Open)
        {
            oracc.Open();
        }
        Transaction = oracc.BeginTransaction();
    }
    public void Rollback()
    {
        WithTransaction = false;
        Transaction.Rollback();
        if (oracc.State != ConnectionState.Closed)
        {
            oracc.Close();
        }
    }
    public void Commit()
    {
        WithTransaction = false;
        Transaction.Commit();
        if (oracc.State != ConnectionState.Closed)
        {
            oracc.Close();
        }
    }
    public object ExecuteScalar(string sql, OracleParameter[] @params)
    {
        OracleConnection cnn = new OracleConnection(connectionstring);
        OracleCommand cmd = new OracleCommand(sql, cnn);
        for (int i = 0; i <= @params.Length - 1; i++)
        {
            cmd.Parameters.Add(@params[i]);
        }

        if (cnn.State != ConnectionState.Open)
        {
            cnn.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        object retval = cmd.ExecuteScalar();
        if ((!WithTransaction))
        {
            cnn.Close();
        }

        return retval;
    }
    public object ExecuteScalar(string sql)
    {
        oracc = new OracleConnection(connectionstring);
        OracleCommand cmd = new OracleCommand(sql, oracc);
        if (oracc.State != ConnectionState.Open)
        {
            oracc.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        object retval = cmd.ExecuteScalar();
        if ((!WithTransaction))
        {
            oracc.Close();
        }
        return retval;
    }
    public DataTable ExecuteDataTable(string sql)
    {
        DataTable dt = new DataTable();
        OracleDataAdapter da = new OracleDataAdapter(sql, oracc);
        da.Fill(dt);
        return dt;
    }
    public DataTable ExecuteDataTable(string sql, OracleParameter[] @params)
    {
        DataTable dt = new DataTable();
        OracleDataAdapter da = new OracleDataAdapter(sql, connectionstring);
        for (int i = 0; i <= @params.Length - 1; i++)
        {
            da.SelectCommand.Parameters.Add(@params[i]);
        }
        da.Fill(dt);
        return dt;
    }
    public DataSet ExecuteDataSet(string sql)
    {
        DataSet ds = new DataSet();
        OracleDataAdapter da = new OracleDataAdapter(sql, connectionstring);
        da.Fill(ds);

        return ds;
    }
    public int excute_PRC(string PRC, OracleParameter[] @params)
    {
        OracleCommand cmd = new OracleCommand();
        cmd.Connection = oracc;
        cmd.CommandType = CommandType.StoredProcedure;
        cmd.CommandText = PRC;

        for (int i = 0; i <= @params.Length - 1; i++)
        {
            cmd.Parameters.Add(@params[i]);
        }

        if (oracc.State != ConnectionState.Open)
        {
            oracc.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        int retval = cmd.ExecuteNonQuery();
        if ((!WithTransaction))
        {
            oracc.Close();
        }
        return retval;
    }
}
public class MyADO
{
    private string connectionstring;
    public SqlConnection cnn = new SqlConnection();
    private bool WithTransaction;
    private SqlTransaction _transaction;

    public MyADO(string ConnectionName)
    {
        WithTransaction = false;
        if (!string.IsNullOrEmpty(ConnectionName))
        {
            //name provided so search for that connection
            connectionstring = System.Configuration.ConfigurationManager.AppSettings.Get(ConnectionName).ToString();
        }
        //return the value    
        cnn = new SqlConnection(connectionstring);
    }

    //public string GetUserName()
    //{
    //    string str = cnn.ConnectionString.ToLower;
    //    //Data Source=.;Initial Catalog=Dat_50_T;Persist Security Info=True;User ID=sa;Password=ilhsmms 
    //    return str.Substring(str.IndexOf("user id=")).Remove(0, 8).Split(";")(0);
    //}
    //public object GetPassword()
    //{
    //    string str = cnn.ConnectionString.ToLower;
    //    return str.Substring(str.IndexOf("password=")).Remove(0, 9).Split(";")(0);
    //}

    public SqlTransaction Transaction
    {
        get { return this._transaction; }
        set { this._transaction = value; }
    }
    public int ExecuteNonQuery(string sql)
    {
        SqlCommand cmd = new SqlCommand(sql, cnn);
        if (cnn.State != ConnectionState.Open)
        {
            cnn.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        int retval = cmd.ExecuteNonQuery();
        if ((!WithTransaction))
        {
            cnn.Close();
        }
        return retval;
    }
    public int ExecuteNonQuery(string sql, SqlParameter[] @params)
    {
        SqlCommand cmd = new SqlCommand(sql, cnn);
        for (int i = 0; i <= @params.Length - 1; i++)
        {
            cmd.Parameters.Add(@params[i]);
        }

        if (cnn.State != ConnectionState.Open)
        {
            cnn.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        int retval = cmd.ExecuteNonQuery();
        if ((!WithTransaction))
        {
            cnn.Close();
        }
        return retval;
    }
    public void BeginTransaction()
    {
        WithTransaction = true;
        if (cnn.State != ConnectionState.Open)
        {
            cnn.Open();
        }
        Transaction = cnn.BeginTransaction();
    }
    public void Rollback()
    {
        WithTransaction = false;
        Transaction.Rollback();
        if (cnn.State != ConnectionState.Closed)
        {
            cnn.Close();
        }
    }
    public void Commit()
    {
        WithTransaction = false;
        Transaction.Commit();
        if (cnn.State != ConnectionState.Closed)
        {
            cnn.Close();
        }
    }
    public object ExecuteScalar(string sql, SqlParameter[] @params)
    {
        SqlConnection cnn = new SqlConnection(connectionstring);
        SqlCommand cmd = new SqlCommand(sql, cnn);
        for (int i = 0; i <= @params.Length - 1; i++)
        {
            cmd.Parameters.Add(@params[i]);
        }

        if (cnn.State != ConnectionState.Open)
        {
            cnn.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        object retval = cmd.ExecuteScalar();
        if ((!WithTransaction))
        {
            cnn.Close();
        }

        return retval;
    }
    public object ExecuteScalar(string sql)
    {
        cnn = new SqlConnection(connectionstring);
        SqlCommand cmd = new SqlCommand(sql, cnn);
        if (cnn.State != ConnectionState.Open)
        {
            cnn.Open();
        }
        if ((WithTransaction))
        {
            cmd.Transaction = Transaction;
        }
        object retval = cmd.ExecuteScalar();
        if ((!WithTransaction))
        {
            cnn.Close();
        }
        return retval;
    }
    public DataTable ExecuteDataTable(string sql)
    {
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, cnn);
        da.Fill(dt);
        return dt;
    }
    public DataTable ExecuteDataTable(string sql, SqlParameter[] @params)
    {
        DataTable dt = new DataTable();
        SqlDataAdapter da = new SqlDataAdapter(sql, connectionstring);
        for (int i = 0; i <= @params.Length - 1; i++)
        {
            da.SelectCommand.Parameters.Add(@params[i]);
        }
        da.Fill(dt);
        return dt;
    }
    public DataSet ExecuteDataSet(string sql)
    {
        DataSet ds = new DataSet();
        SqlDataAdapter da = new SqlDataAdapter(sql, connectionstring);
        da.Fill(ds);

        return ds;
    }
}